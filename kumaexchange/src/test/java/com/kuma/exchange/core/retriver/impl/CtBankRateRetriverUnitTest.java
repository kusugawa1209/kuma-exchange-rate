package com.kuma.exchange.core.retriver.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.util.StringUtils;

import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.utils.ApplicationContextUtils;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class CtBankRateRetriverUnitTest {

	private AbstractRateRetriver rateRetriver;

	@Before
	public void init() {
		this.rateRetriver = ApplicationContextUtils.getBean(CtBankRateRetriver.class);
	}

	@Test
	public void testRetriveRate() {
		List<ExchangeRate> rates = rateRetriver.retriveRates();
		String retriverName = StringUtils.substringBefore(rateRetriver.getClass().getSimpleName(), "$$");
		Assert.assertTrue(retriverName + " 至少應該取得一筆匯率資料", CollectionUtils.isNotEmpty(rates));
	}

}
