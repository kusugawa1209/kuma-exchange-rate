package com.kuma.exchange.properties;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.kuma.exchange.message.view.RateInfoView;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Component
@ConfigurationProperties("app.message")
public class MessageProperties {

	private String invalidCommand;

	private String helpCommand;

	private String setupPrefix;

	@Setter(AccessLevel.PRIVATE)
	private String setupSplit = " ";

	private String rateNotSupportedPattern;

	private String supportedBanks;

	private String supportedCommands;

	private String supportedCommandsFooter;

	private String users;

	private String transferAdminExample;

	private String messagesFromUserPattern;

	private String messageTransferSuccess;

	private String messageTransferSuccessPattern;

	private String transferToUserSuccessPattern;

	private String transferUserExample;

	private String rateExampleHeader;

	private String rateExampleDescription;

	private List<RateInfoView> rateExamples;

	private List<String> rateExampleFooters;

	private String cashBuy;

	private String cashSell;

	private String spotBuy;

	private String spotSell;

	private String rateNotFound;

	private String rateZero;

	private String emptyRates;

	private String separator;

	private String rateInfoMessage;

	private String moreInfo;

	private String lastUpdate;

	private String lastUpdateDatePattern;

	private String rateInfoDatePattern;

	private String showMoreCurrencies;

	private String showMoreRatesPattern;

	private String rateInfo;

	private String updateSuccess;

	private String rateUpdated;

	private String showRateCommand;

	private String showAllCommands;

	private String transferToApp;

	private String transferToAppPattern;

	private String themeUpdated;

	private String themedChangedPattern;

	private String changeOtherTheme;

	private String headerText;

	private String headerDescription;

	private String body;

	private String button;

	private String chooseThisTheme;

	private String totalUsersCountPattern;
}
