package com.kuma.exchange.properties;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.kuma.exchange.core.vo.FlexMessageStyles;

import lombok.Data;

@Data
@Component
@ConfigurationProperties("app.theme")
public class ThemeProperties {
	List<FlexMessageStyles> themes;

	public Optional<FlexMessageStyles> getById(Long id) {
		if (id == null) {
			return Optional.empty();
		}

		return this.themes.stream().filter(theme -> theme.getId().equals(id)).findFirst();
	}

	public FlexMessageStyles getDefaultStyle() {
		return this.themes.get(0);
	}
}
