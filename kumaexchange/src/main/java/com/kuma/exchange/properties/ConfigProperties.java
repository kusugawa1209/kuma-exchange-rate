package com.kuma.exchange.properties;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties("app.config")
public class ConfigProperties {

	public Path getDownloadPath() {
		File downloadDir = Paths.get(workspace, downloadDirectory).toFile();
		if (!downloadDir.exists()) {
			downloadDir.mkdirs();
		}

		return downloadDir.toPath();
	}

	public Path getStickerPath() {
		File stickersDir = Paths.get(workspace, stickerDirectory).toFile();
		if (!stickersDir.exists()) {
			stickersDir.mkdirs();
		}

		return stickersDir.toPath();
	}

	private String workspace;

	private String downloadDirectory;

	private String stickerDirectory;
}
