package com.kuma.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EntityScan
@EnableScheduling
@EnableJpaRepositories
@SpringBootApplication
@EnableConfigurationProperties
public class ExchangeRateApp {

	public static void main(String[] args) {
		SpringApplication.run(ExchangeRateApp.class, args);
	}
}
