package com.kuma.exchange.message.view;

import lombok.Data;

@Data
public class RateInfoView {
	private String commandSample;

	private String commandDescription;

	private String command;
}
