package com.kuma.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.kuma.exchange.properties.ConfigProperties;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ExchangeRateAppMvcConfigurer extends WebMvcConfigurerAdapter {

	@Autowired
	private ConfigProperties configProperties;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		String downloadedContentUri = configProperties.getDownloadPath().toUri().toASCIIString();
		log.info("downloaded dir: {}", downloadedContentUri);
		registry.addResourceHandler("/downloaded/**").addResourceLocations(downloadedContentUri);
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
	}
}
