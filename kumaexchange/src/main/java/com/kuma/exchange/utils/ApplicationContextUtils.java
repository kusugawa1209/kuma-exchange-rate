package com.kuma.exchange.utils;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextUtils implements ApplicationContextAware {

	private static ApplicationContext _applicationContext = null;

	public void setApplicationContext(ApplicationContext applicationContext) {
		_applicationContext = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {

		if (_applicationContext == null) {
			throw new NoSuchBeanDefinitionException("may not register 'ApplicationContextUtils' to spring bean.");
		}

		return _applicationContext;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(String beanName) {
		return (T) _applicationContext.getBean(beanName);
	}

	public static <T> T getBean(Class<T> requiredType) {
		return _applicationContext.getBean(requiredType);
	}

}
