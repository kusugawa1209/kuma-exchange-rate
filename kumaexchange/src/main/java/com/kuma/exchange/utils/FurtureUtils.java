package com.kuma.exchange.utils;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FurtureUtils {

	public static <T> CompletableFuture<List<T>> sequence(Collection<CompletableFuture<T>> futures) {
		return CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]))
				.thenApply(v -> futures.stream().map(CompletableFuture::join).collect(Collectors.toList()));
	}

	public static <T> CompletableFuture<List<T>> sequence(Stream<CompletableFuture<T>> futureStream) {
		List<CompletableFuture<T>> futures = futureStream.collect(Collectors.toList());
		return sequence(futures);
	}

}
