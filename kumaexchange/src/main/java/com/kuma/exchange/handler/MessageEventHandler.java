package com.kuma.exchange.handler;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.MessageContent;

public interface MessageEventHandler<T extends MessageContent> {
	
	public Class<T> getSupportedMessageContentType();
	
	public void handleReply(MessageEvent<?> event);
	
	public void redirectMessageToAdmins(MessageEvent<?> event);
}
