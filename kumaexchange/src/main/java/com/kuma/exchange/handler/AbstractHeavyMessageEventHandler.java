package com.kuma.exchange.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.vo.DownloadedContent;
import com.kuma.exchange.enums.HeavyContentExtensionEnum;
import com.kuma.exchange.enums.MessageContentTypeEnum;
import com.kuma.exchange.properties.ConfigProperties;
import com.kuma.utils.LogUtils;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.MessageContentResponse;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.MessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@LineMessageHandler
public abstract class AbstractHeavyMessageEventHandler<T extends MessageContent>
		extends AbstractMessageEventHandler<T> {

	protected abstract HeavyContentExtensionEnum getSupportedExtension();

	protected abstract MessageContentTypeEnum getMessageContentType();

	protected abstract Message createContentMessage(DownloadedContent content) throws IOException;

	@Autowired
	private LineMessagingClient lineMessagingClient;

	@Autowired
	private ConfigProperties configProperties;

	@Override
	protected List<Message> generateMessagesToAdmins(MessageEvent<?> event) {
		MessageContent messageContent = event.getMessage();
		try {
			MessageContentResponse response = lineMessagingClient.getMessageContent(messageContent.getId()).get();
			DownloadedContent downloadedContent = saveContent(getSupportedExtension().name().toLowerCase(), response);

			Follower eventSender = getEventSender(event);
			String headerText = createHeaderText(eventSender);
			String bodyText = String.format(messageProperties.getTransferToAppPattern(), eventSender.getDisplayName(),
					getMessageContentType().getContentDisplayName());

			FlexMessage infoMessage = flexMessageHelper.createFlexMessage(headerText, headerText, bodyText,
					Optional.empty());

			Message contentMessage = createContentMessage(downloadedContent);

			return Lists.newArrayList(infoMessage, contentMessage);
		} catch (InterruptedException | ExecutionException | IOException e) {
			LogUtils.logThrowable(e);
			botMessageService.sendErrorLogToAdmins(e);
			Thread.currentThread().interrupt();

			return Lists.newArrayList();
		}
	}

	private DownloadedContent saveContent(String ext, MessageContentResponse responseBody) throws IOException {
		log.info("Got content-type: {}", responseBody);

		DownloadedContent tempFile = createTempFile(ext);
		try (OutputStream outputStream = Files.newOutputStream(tempFile.getPath())) {
			ByteStreams.copy(responseBody.getStream(), outputStream);
			log.info("Saved {}: {}", ext, tempFile);
			return tempFile;
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private DownloadedContent createTempFile(String ext) throws IOException {
		String dateString = DateFormatUtils.format(new Date(), "yyyy-MM-dd_HH-mm-ss-SSS");
		String fileName = dateString + '-' + UUID.randomUUID().toString() + '.' + ext;
		Path tempFile = configProperties.getDownloadPath().resolve(fileName);
		tempFile.toFile().deleteOnExit();
		if (!tempFile.toFile().getParentFile().exists()) {
			tempFile.toFile().getParentFile().mkdirs();
		}
		if (!tempFile.toFile().createNewFile()) {
			log.error("Can' create temp file...");
		}

		return new DownloadedContent(tempFile, createUri("/downloaded/" + tempFile.getFileName()));
	}

	private String createUri(String path) {
		return ServletUriComponentsBuilder.fromCurrentContextPath().path(path).build().toUriString();
	}
}
