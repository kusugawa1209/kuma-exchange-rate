package com.kuma.exchange.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.helper.message.FlexMessageHelper;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.BotMessageService;
import com.kuma.exchange.core.service.ExchangeRateService;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.properties.MessageProperties;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.MessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;

public abstract class AbstractMessageEventHandler<T extends MessageContent> implements MessageEventHandler<T> {

	@Autowired
	private ExchangeRateService exchangeRateService;

	@Autowired
	protected BotMessageService botMessageService;

	@Autowired
	private FollowerService followerService;

	@Autowired
	private ExchangeRateMessageHandler exchangeRateMessageHandler;

	@Autowired
	protected FlexMessageHelper flexMessageHelper;

	@Autowired
	protected MessageProperties messageProperties;

	protected abstract List<Message> generateMessagesToAdmins(MessageEvent<?> event);

	@Override
	public void handleReply(MessageEvent<?> event) {
		String userId = event.getSource().getUserId();
		String replyToken = event.getReplyToken();

		handleDefaultReply(replyToken, userId);
	}

	@Override
	public void redirectMessageToAdmins(MessageEvent<?> event) {
		List<Message> messagesToAdmins = generateMessagesToAdmins(event);

		botMessageService.pushMessagesToAdmins(messagesToAdmins);
	}

	protected void handleDefaultReply(String replyToken, String userId) {
		List<ExchangeRate> rates = exchangeRateService.findFirstAvaliableRates();
		List<FlexMessage> rateMessages = exchangeRateMessageHandler.convertToMessages(rates);
		List<Message> themedMessages = flexMessageHelper.convertToThemedMessages(userId,
				Lists.newArrayList(rateMessages));

		botMessageService.replyMessages(replyToken, themedMessages);
	}

	protected String createHeaderText(MessageEvent<?> event) {
		Follower sender = getEventSender(event);

		String senderDisplayName = sender.getDisplayName();

		return senderDisplayName + " " + messageProperties.getTransferToApp();
	}

	protected String createHeaderText(Follower eventSender) {
		String senderDisplayName = eventSender.getDisplayName();

		return senderDisplayName + " " + messageProperties.getTransferToApp();
	}

	protected Follower getEventSender(MessageEvent<?> event) {
		String senderId = event.getSource().getSenderId();
		return followerService.getById(senderId);
	}
}
