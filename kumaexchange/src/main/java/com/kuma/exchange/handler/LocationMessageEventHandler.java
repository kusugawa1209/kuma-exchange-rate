package com.kuma.exchange.handler;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.enums.MessageContentTypeEnum;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.LocationMessage;
import com.linecorp.bot.model.message.Message;

@Component
public class LocationMessageEventHandler extends AbstractMessageEventHandler<LocationMessageContent> {

	@Override
	public Class<LocationMessageContent> getSupportedMessageContentType() {
		return LocationMessageContent.class;
	}

	@Override
	protected List<Message> generateMessagesToAdmins(MessageEvent<?> event) {
		Follower eventSender = getEventSender(event);
		LocationMessageContent messageContent = (LocationMessageContent) event.getMessage();

		String headerText = createHeaderText(eventSender);
		String bodyText = String.format(messageProperties.getTransferToAppPattern(), eventSender.getDisplayName(),
				MessageContentTypeEnum.LOCATION.getContentDisplayName());
		FlexMessage infoMessage = flexMessageHelper.createFlexMessage(headerText, headerText, bodyText,
				Optional.empty());

		LocationMessage locationMessage = new LocationMessage(headerText, messageContent.getAddress(),
				messageContent.getLatitude(), messageContent.getLongitude());

		return Lists.newArrayList(infoMessage, locationMessage);
	}

}
