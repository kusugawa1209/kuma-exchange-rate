package com.kuma.exchange.handler;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.kuma.exchange.core.vo.DownloadedContent;
import com.kuma.exchange.enums.HeavyContentExtensionEnum;
import com.kuma.exchange.enums.MessageContentTypeEnum;
import com.linecorp.bot.model.event.message.AudioMessageContent;
import com.linecorp.bot.model.message.AudioMessage;
import com.linecorp.bot.model.message.Message;
import com.xuggle.xuggler.IContainer;

@Component
public class AudioMessageEventHandler extends AbstractHeavyMessageEventHandler<AudioMessageContent> {

	@Override
	public Class<AudioMessageContent> getSupportedMessageContentType() {
		return AudioMessageContent.class;
	}

	@Override
	protected HeavyContentExtensionEnum getSupportedExtension() {
		return HeavyContentExtensionEnum.MP4;
	}

	@Override
	protected MessageContentTypeEnum getMessageContentType() {
		return MessageContentTypeEnum.AUDIO;
	}

	@Override
	protected Message createContentMessage(DownloadedContent content) throws IOException {
		IContainer container = IContainer.make();
		container.open(content.getPath().toString(), IContainer.Type.READ, null);
		Long duration = container.getDuration() / 1000;
		container.close();

		return new AudioMessage(content.getUri(), duration.intValue());
	}
}
