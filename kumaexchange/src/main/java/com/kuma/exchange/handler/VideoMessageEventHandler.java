package com.kuma.exchange.handler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.kuma.exchange.core.vo.DownloadedContent;
import com.kuma.exchange.enums.HeavyContentExtensionEnum;
import com.kuma.exchange.enums.MessageContentTypeEnum;
import com.linecorp.bot.model.event.message.VideoMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.VideoMessage;

@Component
public class VideoMessageEventHandler extends AbstractHeavyMessageEventHandler<VideoMessageContent> {

	@Override
	public Class<VideoMessageContent> getSupportedMessageContentType() {
		return VideoMessageContent.class;
	}

	@Override
	protected HeavyContentExtensionEnum getSupportedExtension() {
		return HeavyContentExtensionEnum.MP4;
	}

	@Override
	protected MessageContentTypeEnum getMessageContentType() {
		return MessageContentTypeEnum.VIDEO;
	}

	@Override
	protected Message createContentMessage(DownloadedContent content) throws IOException {
		File original = content.getPath().toFile();

		ClassPathResource classPathResource = new ClassPathResource("static/videoThumb.png");

		InputStream inputStream = classPathResource.getInputStream();
		File originalDirectory = original.getParentFile();

		String previewFileName = "videoThumb.png";
		File previewFile = Paths.get(originalDirectory.toPath().toString(), previewFileName).toFile();
		if (!previewFile.exists()) {
			FileUtils.copyInputStreamToFile(inputStream, previewFile);
		}

		String originalFileName = FilenameUtils.getName(original.getName());

		String previewUri = StringUtils.replace(content.getUri(), originalFileName, previewFileName);

		return new VideoMessage(content.getUri(), previewUri);
	}
}
