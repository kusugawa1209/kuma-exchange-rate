package com.kuma.exchange.handler;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.properties.ConfigProperties;
import com.kuma.utils.LogUtils;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Image;

@Component
public class StickerMessageEventHandler extends AbstractMessageEventHandler<StickerMessageContent> {

	@Autowired
	protected ConfigProperties configProperties;

	@Override
	public Class<StickerMessageContent> getSupportedMessageContentType() {
		return StickerMessageContent.class;
	}

	@Override
	protected List<Message> generateMessagesToAdmins(MessageEvent<?> event) {
		String headerText = createHeaderText(event);
		StickerMessageContent messageContent = (StickerMessageContent) event.getMessage();
		String stickerId = messageContent.getStickerId();

		String stickerImageUri = "https://stickershop.line-scdn.net/stickershop/v1/sticker/" + stickerId
				+ "/ANDROID/sticker.png;compress=true";

		Path stickerPath = configProperties.getStickerPath();
		File stickerFile = Paths.get(stickerPath.toString(), stickerId + ".png").toFile();
		if (!stickerFile.exists()) {
			try {
				URL url = new URL(stickerImageUri);
				BufferedImage img = ImageIO.read(url);
				ImageIO.write(img, "png", stickerFile);
			} catch (IOException e) {
				LogUtils.logThrowable(e);
				botMessageService.sendErrorLogToAdmins(e);
			}
		}

		Image image = flexMessageHelper.createImage(stickerImageUri, Optional.empty());

		FlexMessage message = flexMessageHelper.createFlexMessage(headerText, headerText, Lists.newArrayList(image),
				Optional.empty());

		return Lists.newArrayList(message);
	}

}
