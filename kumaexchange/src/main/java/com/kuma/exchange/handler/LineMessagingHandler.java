package com.kuma.exchange.handler;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.kuma.exchange.core.helper.message.FlexMessageHelper;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.BotMessageService;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.core.service.PostBackEventService;
import com.kuma.exchange.properties.MessageProperties;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.PostbackEvent;
import com.linecorp.bot.model.event.UnfollowEvent;
import com.linecorp.bot.model.event.message.AudioMessageContent;
import com.linecorp.bot.model.event.message.ImageMessageContent;
import com.linecorp.bot.model.event.message.LocationMessageContent;
import com.linecorp.bot.model.event.message.StickerMessageContent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.message.VideoMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@LineMessageHandler
public class LineMessagingHandler {

	@Autowired
	private BotMessageService botMessageService;

	@Autowired
	private FollowerService followerService;

	@Autowired
	private PostBackEventService postBackEventService;

	@Autowired
	private MessageProperties messageProperties;

	@Autowired
	private FlexMessageHelper flexMessageHelper;

	@Autowired
	private ApplicationContext applicationContext;

	@EventMapping
	public void handlePostBackEvent(PostbackEvent event) {
		logEvent(event);

		postBackEventService.handleEvent(event);
	}

	@EventMapping
	public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
		handleMessageEvent(event);
	}

	@EventMapping
	public void handleStickerMessageEvent(MessageEvent<StickerMessageContent> event) {
		handleMessageEvent(event);
	}

	@EventMapping
	public void handleLocationrMessageEvent(MessageEvent<LocationMessageContent> event) {
		handleMessageEvent(event);
	}

	@EventMapping
	public void handleImageMessageEvent(MessageEvent<ImageMessageContent> event) {
		handleMessageEvent(event);
	}

	@EventMapping
	public void handleVideoMessageEvent(MessageEvent<VideoMessageContent> event) {
		handleMessageEvent(event);
	}

	@EventMapping
	public void handleAudioMessageEvent(MessageEvent<AudioMessageContent> event) {
		handleMessageEvent(event);
	}

	@EventMapping
	public void handleFollowEvent(FollowEvent event) {
		logEvent(event);

		followerService.handleFollowEvent(event);
	}

	@EventMapping
	public void handleUnfollowEvent(UnfollowEvent event) {
		logEvent(event);

		followerService.handleUnfollowEvent(event);
	}

	@EventMapping
	public void defaultMessageEvent(Event event) {
		logEvent(event);
	}

	private void logEvent(Event e) {
		log.debug("event: {}: {}", e.getClass().getSimpleName(), e);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void handleMessageEvent(MessageEvent<?> event) {
		Map<String, MessageEventHandler> handlers = applicationContext.getBeansOfType(MessageEventHandler.class)
				.entrySet().stream()
				.collect(Collectors.toMap(entry -> entry.getValue().getSupportedMessageContentType().getName(),
						Entry<String, MessageEventHandler>::getValue));

		MessageEventHandler handler = handlers.get(event.getMessage().getClass().getName());
		String userId = event.getSource().getUserId();
		if (handler != null) {
			handler.handleReply(event);

			if (!followerService.isAdmin(userId)) {
				handler.redirectMessageToAdmins(event);
			}
		} else {
			Follower sender = followerService.getById(userId);
			String headerText = createHeaderText(sender);
			FlexMessage infoMessage = flexMessageHelper.createFlexMessage(headerText, headerText,
					"UnsupportedType: " + event.getMessage().getClass().getSimpleName(), Optional.empty());

			botMessageService.pushMessageToAdmins(infoMessage);
		}
	}

	private String createHeaderText(Follower eventSender) {
		String senderDisplayName = eventSender.getDisplayName();

		return senderDisplayName + " " + messageProperties.getTransferToApp();
	}
}
