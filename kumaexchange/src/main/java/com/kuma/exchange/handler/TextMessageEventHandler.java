package com.kuma.exchange.handler;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.service.CommandService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;

@Component
public class TextMessageEventHandler extends AbstractMessageEventHandler<TextMessageContent> {

	private static final String SETUP_PREFIX = "/";

	@Autowired
	private CommandService commandService;

	@Override
	public void handleReply(MessageEvent<?> event) {
		TextMessageContent messageContent = (TextMessageContent) event.getMessage();
		String messageText = messageContent.getText();
		String userId = event.getSource().getUserId();
		String replyToken = event.getReplyToken();

		if (StringUtils.startsWith(messageText, SETUP_PREFIX)) {

			commandService.handleCommand(userId, messageText);
		} else {
			handleDefaultReply(replyToken, userId);
		}
	}

	@Override
	public Class<TextMessageContent> getSupportedMessageContentType() {
		return TextMessageContent.class;
	}

	@Override
	protected List<Message> generateMessagesToAdmins(MessageEvent<?> event) {
		String headerText = createHeaderText(event);
		TextMessageContent messageContent = (TextMessageContent) event.getMessage();
		String messageText = messageContent.getText();

		FlexMessage message = flexMessageHelper.createFlexMessage(headerText, headerText, messageText,
				Optional.empty());

		return Lists.newArrayList(message);
	}

}
