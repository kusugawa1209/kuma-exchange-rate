package com.kuma.exchange.handler;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.helper.message.FlexMessageHelper;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.properties.MessageProperties;
import com.kuma.exchange.properties.ThemeProperties;
import com.kuma.exchange.web.view.CommandPostBackView;
import com.kuma.exchange.web.view.ExchangeRatePostBackView;
import com.kuma.utils.JsonUtils;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;

@Component
public class ExchangeRateMessageHandler {

	private static final int MAX_FRACTION_DIGITS = 8;

	private static final int RATE_VALUE_MAX_LENGTH = 6;

	@Autowired
	private MessageProperties messageProperties;

	@Autowired
	private ThemeProperties themeProperties;

	@Autowired
	private FlexMessageHelper flexMessageHelper;

	public List<FlexMessage> convertToMessages(Collection<ExchangeRate> rates) {
		if (CollectionUtils.isEmpty(rates)) {
			return Lists.newArrayList(flexMessageHelper.createFlexMessage(messageProperties.getRateNotFound(),
					messageProperties.getRateNotFound(), messageProperties.getRateNotFound(),
					Optional.of(themeProperties.getDefaultStyle())));
		} else {
			List<Bubble> rateBubbles = rates.stream().map((ExchangeRate rate) -> {
				Box header = generateHeader(rate);
				Box body = generateBody(rate);
				Box footer = generateFooter(rate);

				return flexMessageHelper.createBubble(header, null, body, footer,
						Optional.of(themeProperties.getDefaultStyle()));
			}).collect(Collectors.toList());

			return flexMessageHelper.createFlexMessages(messageProperties.getRateInfo(), rateBubbles);
		}
	}

	private Box generateHeader(ExchangeRate rate) {
		List<FlexComponent> contents = Lists.newArrayList();

		String rateInfoText = rate.getRateFullDisplayName();
		Text headerText = flexMessageHelper.createHeaderText(rateInfoText,
				Optional.of(themeProperties.getDefaultStyle()));
		contents.add(headerText);

		String lastUpdateDateText = DateFormatUtils.format(rate.getCreationDate(),
				messageProperties.getLastUpdateDatePattern());
		String lastUpdateText = messageProperties.getLastUpdate() + lastUpdateDateText;
		Text headerDescription = flexMessageHelper.createHeaderDescriptionText(lastUpdateText,
				Optional.of(themeProperties.getDefaultStyle()));
		contents.add(headerDescription);

		return flexMessageHelper.createBox(contents, FlexLayout.VERTICAL);
	}

	private Box generateBody(ExchangeRate rate) {
		List<FlexComponent> contents = Lists.newArrayList();

		Text cashBuyText = flexMessageHelper.createBodyText(messageProperties.getCashBuy());
		Text cashBuyValue = flexMessageHelper.createBodyText(paddingZero(rate.getCashBuy()));
		List<FlexComponent> cashBuyComponents = Lists.newArrayList(cashBuyText, cashBuyValue);
		Box cashBuyBox = flexMessageHelper.createBox(cashBuyComponents, FlexLayout.HORIZONTAL);
		contents.add(cashBuyBox);

		Text cashSellText = flexMessageHelper.createBodyText(messageProperties.getCashSell());
		Text cashSellValue = flexMessageHelper.createBodyText(paddingZero(rate.getCashSell()));
		List<FlexComponent> cashSellComponents = Lists.newArrayList(cashSellText, cashSellValue);
		Box cashSellBox = flexMessageHelper.createBox(cashSellComponents, FlexLayout.HORIZONTAL);
		contents.add(cashSellBox);

		Text spotBuyText = flexMessageHelper.createBodyText(messageProperties.getSpotBuy());
		Text spotBuyValue = flexMessageHelper.createBodyText(paddingZero(rate.getSpotBuy()));
		List<FlexComponent> spotBuyComponents = Lists.newArrayList(spotBuyText, spotBuyValue);
		Box spotBuyBox = flexMessageHelper.createBox(spotBuyComponents, FlexLayout.HORIZONTAL);
		contents.add(spotBuyBox);

		Text spotSellText = flexMessageHelper.createBodyText(messageProperties.getSpotSell());
		Text spotSellValue = flexMessageHelper.createBodyText(paddingZero(rate.getSpotSell()));
		List<FlexComponent> spotSellComponents = Lists.newArrayList(spotSellText, spotSellValue);
		Box spotSellBox = flexMessageHelper.createBox(spotSellComponents, FlexLayout.HORIZONTAL);
		contents.add(spotSellBox);

		return flexMessageHelper.createBox(contents, FlexLayout.VERTICAL);
	}

	private Box generateFooter(ExchangeRate rate) {
		List<FlexComponent> contents = Lists.newArrayList();

		URIAction moreCurrenciesAction = new URIAction(messageProperties.getShowMoreCurrencies(),
				rate.getBank().getUri());
		Button moreCurrenciesButton = flexMessageHelper.createButton(moreCurrenciesAction,
				Optional.of(themeProperties.getDefaultStyle()));
		contents.add(moreCurrenciesButton);

		ExchangeRatePostBackView exchangeRatePostBack = new ExchangeRatePostBackView();
		exchangeRatePostBack.setParams(Lists.newArrayList(rate.getCurrencyCode()));

		String showMoreRatesLabel = String.format(messageProperties.getShowMoreRatesPattern(), rate.getDisplayName());
		PostbackAction moreRatesAction = new PostbackAction(showMoreRatesLabel,
				JsonUtils.toJsonString(exchangeRatePostBack));
		Button moreRatesButton = flexMessageHelper.createButton(moreRatesAction,
				Optional.of(themeProperties.getDefaultStyle()));
		contents.add(moreRatesButton);

		ExchangeRatePostBackView rateCommandPostBack = new ExchangeRatePostBackView();
		rateCommandPostBack.setParams(Lists.newArrayList(messageProperties.getHelpCommand()));
		PostbackAction rateCommandAction = new PostbackAction(messageProperties.getShowRateCommand(),
				JsonUtils.toJsonString(rateCommandPostBack));
		Button rateCommandButton = flexMessageHelper.createButton(rateCommandAction,
				Optional.of(themeProperties.getDefaultStyle()));
		contents.add(rateCommandButton);

		CommandPostBackView allCommandPostBack = new CommandPostBackView();
		allCommandPostBack.setParams(CommandEnum.HELP);
		PostbackAction allCommandAction = new PostbackAction(messageProperties.getShowAllCommands(),
				JsonUtils.toJsonString(allCommandPostBack));
		Button allCommandButton = flexMessageHelper.createButton(allCommandAction,
				Optional.of(themeProperties.getDefaultStyle()));
		contents.add(allCommandButton);

		return flexMessageHelper.createBox(contents, FlexLayout.VERTICAL);
	}

	private String paddingZero(Double rate) {
		if (rate == null) {
			return messageProperties.getRateNotFound();
		}

		DecimalFormat df = new DecimalFormat("#.#");

		df.setMaximumFractionDigits(MAX_FRACTION_DIGITS);

		String rateValue = df.format(rate);
		if (!StringUtils.contains(rateValue, ".")) {
			rateValue += ".0";
		}

		rateValue = StringUtils.rightPad(rateValue, RATE_VALUE_MAX_LENGTH, "0");

		if (StringUtils.equals(rateValue, messageProperties.getRateZero())) {
			return messageProperties.getRateNotFound();
		} else {
			return rateValue;
		}
	}

}
