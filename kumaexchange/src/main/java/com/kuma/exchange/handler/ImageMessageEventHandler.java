package com.kuma.exchange.handler;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.kuma.exchange.core.vo.DownloadedContent;
import com.kuma.exchange.enums.HeavyContentExtensionEnum;
import com.kuma.exchange.enums.MessageContentTypeEnum;
import com.linecorp.bot.model.event.message.ImageMessageContent;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.Message;

@Component
public class ImageMessageEventHandler extends AbstractHeavyMessageEventHandler<ImageMessageContent> {

	private static final int PREVIEW_IMAGE_SIZE = 320;

	@Override
	public Class<ImageMessageContent> getSupportedMessageContentType() {
		return ImageMessageContent.class;
	}

	@Override
	protected HeavyContentExtensionEnum getSupportedExtension() {
		return HeavyContentExtensionEnum.JPG;
	}

	@Override
	protected MessageContentTypeEnum getMessageContentType() {
		return MessageContentTypeEnum.IMAGE;
	}

	@Override
	protected Message createContentMessage(DownloadedContent content) throws IOException {
		File original = content.getPath().toFile();
		BufferedImage image = ImageIO.read(original);
		BufferedImage resized = resize(image);

		String originalBaseName = FilenameUtils.getBaseName(original.getName());
		String originalExtension = FilenameUtils.getExtension(original.getName());
		File originalDirectory = original.getParentFile();

		String previewFileName = originalBaseName + "-preview." + originalExtension;
		File preview = Paths.get(originalDirectory.toPath().toString(), previewFileName).toFile();
		ImageIO.write(resized, "png", preview);

		String originalFileName = FilenameUtils.getName(original.getName());
		String previewUri = StringUtils.replace(content.getUri(), originalFileName, previewFileName);

		return new ImageMessage(content.getUri(), previewUri);
	}

	private static BufferedImage resize(BufferedImage img) {
		int imgWidth = img.getWidth();
		int imgHeight = img.getHeight();

		if (imgWidth < PREVIEW_IMAGE_SIZE && imgHeight < PREVIEW_IMAGE_SIZE) {
			return img;
		}

		int previewWidth = imgWidth > imgHeight ? PREVIEW_IMAGE_SIZE : PREVIEW_IMAGE_SIZE * imgWidth / imgHeight;
		int previewHeight = imgWidth > imgHeight ? PREVIEW_IMAGE_SIZE * imgHeight / imgWidth : PREVIEW_IMAGE_SIZE;

		Image tmp = img.getScaledInstance(previewWidth, previewHeight, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(previewWidth, previewHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();

		return resized;
	}
}
