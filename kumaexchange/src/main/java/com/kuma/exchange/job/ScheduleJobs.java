package com.kuma.exchange.job;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.core.service.BotMessageService;
import com.kuma.exchange.core.service.ExchangeRateService;
import com.kuma.exchange.handler.ExchangeRateMessageHandler;
import com.kuma.exchange.properties.ConfigProperties;
import com.kuma.utils.LogUtils;
import com.linecorp.bot.model.message.FlexMessage;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ScheduleJobs {

	@Autowired
	private ExchangeRateService exchangeRateService;

	@Autowired
	private BotMessageService botMessageService;

	@Autowired
	private ExchangeRateMessageHandler exchangeRateHandler;

	@Autowired
	private ConfigProperties configProperties;

	// a week in mills
	private static final int EXPIRED_DURATION = 1000 * 60 * 60 * 24 * 7;

	@Async
	@Scheduled(cron = "0 0 9-21 ? *  MON-FRI")
	public void sendRateInfoToAllUsers() {
		exchangeRateService.updateRates();

		List<ExchangeRate> rates = exchangeRateService.findFirstAvaliableRates();
		List<FlexMessage> rateMessages = exchangeRateHandler.convertToMessages(rates);
		botMessageService.pushMessagesToAllUsers(Lists.newArrayList(rateMessages));

		log.debug("Push message finished.");
	}

	@Async
	@Scheduled(cron = "0 0 0 * * ?")
	public void cleanExpiredDownloadContents() {
		Path downloadPath = configProperties.getDownloadPath();

		long currentTimeMill = new Date().getTime();

		try {
			Files.find(downloadPath, 1, (path, attrs) -> {
				long fileCreationTimeMill = attrs.creationTime().toMillis();
				return currentTimeMill - fileCreationTimeMill > EXPIRED_DURATION;
			}, FileVisitOption.FOLLOW_LINKS).forEach(path -> {
				log.info("{} is expired, therefore delete it.", path.toString());
				FileUtils.deleteQuietly(path.toFile());
			});
		} catch (IOException e) {
			LogUtils.logThrowable(e);
			botMessageService.sendErrorLogToAdmins(e);
		}
	}
}
