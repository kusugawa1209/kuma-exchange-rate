package com.kuma.exchange.web.view;

import com.kuma.exchange.enums.PostBackTypeEnum;

public interface PostBackView<T> {

	public PostBackTypeEnum getEventType();

	public T getParams();
}
