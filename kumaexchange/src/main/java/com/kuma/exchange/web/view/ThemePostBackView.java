package com.kuma.exchange.web.view;

import com.kuma.exchange.enums.PostBackTypeEnum;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ThemePostBackView implements PostBackView<ThemeChoiceView> {

	@Getter
	private PostBackTypeEnum eventType = PostBackTypeEnum.THEME;

	@Getter
	@NonNull
	private ThemeChoiceView params;

}
