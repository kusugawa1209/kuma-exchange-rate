package com.kuma.exchange.web.view;

import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.enums.PostBackTypeEnum;

import lombok.Getter;
import lombok.Setter;

public class CommandPostBackView implements PostBackView<CommandEnum> {

	@Getter
	private PostBackTypeEnum eventType = PostBackTypeEnum.COMMAND;

	@Getter
	@Setter
	private CommandEnum params;
}