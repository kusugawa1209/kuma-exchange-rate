package com.kuma.exchange.web.view;

import com.kuma.exchange.enums.PostBackTypeEnum;

import lombok.Getter;
import lombok.Setter;

public class BasicPostBackView implements PostBackView<Object> {

	@Getter
	private PostBackTypeEnum eventType = PostBackTypeEnum.EXCHANGE_RATE;

	@Getter
	@Setter
	private Object params;
}