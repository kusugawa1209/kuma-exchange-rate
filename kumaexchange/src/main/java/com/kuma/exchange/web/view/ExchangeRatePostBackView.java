package com.kuma.exchange.web.view;

import java.util.List;

import com.google.common.collect.Lists;
import com.kuma.exchange.enums.PostBackTypeEnum;

import lombok.Getter;
import lombok.Setter;

public class ExchangeRatePostBackView implements PostBackView<List<String>> {

	@Getter
	private PostBackTypeEnum eventType = PostBackTypeEnum.EXCHANGE_RATE;

	@Getter
	@Setter
	private List<String> params = Lists.newArrayList();
}