package com.kuma.exchange.web.view;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ThemeChoiceView {

	private Long id;

	private boolean isChoose;
}
