package com.kuma.exchange.core.service.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kuma.exchange.core.dao.HistoryRateDao;
import com.kuma.exchange.core.model.HistoryRate;
import com.kuma.exchange.core.service.HistoryRateService;

@Service
@Transactional
public class HistoryRateServiceImpl implements HistoryRateService {

	@Autowired
	private HistoryRateDao historyRateDao;

	@Override
	public List<HistoryRate> saveHistoryRates(Collection<HistoryRate> historyRates) {
		return historyRateDao.save(historyRates);
	}
}
