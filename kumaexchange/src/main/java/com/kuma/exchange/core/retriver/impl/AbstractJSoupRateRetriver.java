package com.kuma.exchange.core.retriver.impl;

import java.io.IOException;
import java.util.List;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.utils.LogUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public abstract class AbstractJSoupRateRetriver extends AbstractRateRetriver {

	private static final int RETRIVE_TIMEOUT_MILLS = 120_000;

	protected abstract List<ExchangeRate> tryGetExchangeRates(Document document) throws IOException;

	@Override
	protected List<ExchangeRate> getExchangeRates() throws IOException {
		Response response = Jsoup.connect(getBank().getUri()).ignoreContentType(true)
				.userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
				.referrer("http://www.google.com").timeout(RETRIVE_TIMEOUT_MILLS).followRedirects(true).execute();

		Document document = response.parse();

		return tryGetExchangeRates(document);
	}

	@Override
	public List<ExchangeRate> retriveRates() {
		log.info("Start retriving rates of {}", getBank().getDisplayName());
		List<ExchangeRate> rates = Lists.newArrayList();
		int tryTime = 0;
		boolean success = false;

		while (tryTime < MAX_RETRY_TIME && !success) {
			try {
				log.debug("Try for {} time", tryTime + 1);
				rates = getExchangeRates();
				success = true;
				log.debug("Try for {} time success", tryTime + 1);
			} catch (IOException e) {
				tryTime++;
				success = false;
				LogUtils.logThrowable(e, "Try for {} time failed", tryTime + 1);
			}
		}

		if (!success) {
			log.error("Retry over than {} times, give up.", MAX_RETRY_TIME);
		}

		return rates;
	}
}
