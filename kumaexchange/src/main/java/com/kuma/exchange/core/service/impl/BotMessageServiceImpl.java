package com.kuma.exchange.core.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.helper.message.FlexMessageHelper;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.BotMessageService;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.properties.ThemeProperties;
import com.kuma.utils.LogUtils;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.response.BotApiResponse;

@Service
public class BotMessageServiceImpl implements BotMessageService {

	@Autowired
	private FollowerService followerService;

	@Autowired
	private LineMessagingClient lineMessagingClient;

	@Autowired
	private FlexMessageHelper flexMessageHelper;

	@Autowired
	private ThemeProperties themeProperties;

	@Override
	public void replyMessage(String replyToken, Message message) {
		ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
		CompletableFuture<BotApiResponse> future = lineMessagingClient.replyMessage(replyMessage)
				.whenComplete((BotApiResponse r, Throwable t) -> pushMessageToAdmins(message));

		handleFuture(future);
	}

	@Override
	public void replyMessages(String replyToken, List<Message> messages) {
		ReplyMessage replyMessage = new ReplyMessage(replyToken, Lists.newArrayList(messages));
		CompletableFuture<BotApiResponse> future = lineMessagingClient.replyMessage(replyMessage);

		handleFuture(future);
	}

	@Override
	public void pushMessage(String id, Message message) {
		Message themedMessage = flexMessageHelper.convertToThemedMessage(id, message);
		pushLineMessageToUser(id, themedMessage);
	}

	@Override
	public void pushMessageToAdmins(Message message) {
		List<String> adminIds = followerService.findAdmins().stream().map(Follower::getId).collect(Collectors.toList());
		if (message instanceof FlexMessage) {
			FlexMessage flexMessage = (FlexMessage) message;
			Map<String, Message> messages = flexMessageHelper.convertToThemedMessages(adminIds, flexMessage);

			messages.entrySet().stream()
					.forEach(messageEntry -> pushLineMessageToUser(messageEntry.getKey(), messageEntry.getValue()));
		} else {
			adminIds.forEach(adminId -> pushLineMessageToUser(adminId, message));
		}
	}

	@Override
	public void pushMessageToAllUsers(Message message) {
		List<String> followerIds = followerService.findAll().stream().map(Follower::getId).collect(Collectors.toList());
		Map<String, Message> themedMessages = flexMessageHelper.convertToThemedMessages(followerIds, message);
		themedMessages.entrySet().forEach(entry -> pushLineMessageToUser(entry.getKey(), entry.getValue()));
	}

	@Override
	public void pushMessages(String id, List<Message> messages) {
		List<Message> themedMessages = flexMessageHelper.convertToThemedMessages(id, messages);

		pushLineMessagesToUser(id, themedMessages);
	}

	@Override
	public void pushThemeMessages(String id, List<Message> messages) {
		pushLineMessagesToUser(id, messages);
	}

	@Override
	public void pushMessagesToAdmins(List<Message> messages) {
		List<String> adminIds = followerService.findAdmins().stream().map(Follower::getId).collect(Collectors.toList());

		Map<String, List<Message>> themedMessages = flexMessageHelper.convertToThemedMessages(adminIds, messages);

		themedMessages.entrySet().forEach(entry -> pushLineMessagesToUser(entry.getKey(), entry.getValue()));
	}

	@Override
	public void pushMessagesToAllUsers(List<Message> messages) {
		List<String> allUserIds = followerService.findAll().stream().map(Follower::getId).collect(Collectors.toList());
		Map<String, List<Message>> themedMessages = flexMessageHelper.convertToThemedMessages(allUserIds, messages);

		themedMessages.entrySet().forEach(entry -> pushLineMessagesToUser(entry.getKey(), entry.getValue()));
	}

	@Override
	public void sendErrorLogToAdmins(Throwable t) {
		String stackTrace = ExceptionUtils.getStackTrace(t);
		String trimedStackTrace = StringUtils.substring(stackTrace, 0, 2000);

		FlexMessage message = flexMessageHelper.createFlexMessage("oops...", "oops...", trimedStackTrace,
				Optional.of(themeProperties.getDefaultStyle()));

		pushMessageToAdmins(message);
	}

	private <M extends Message> void pushLineMessagesToUser(String id, List<M> messages) {
		List<Message> lineMessages = Lists.newArrayList(messages);
		CompletableFuture<BotApiResponse> future = lineMessagingClient.pushMessage(new PushMessage(id, lineMessages));

		handleFuture(future);
	}

	private void pushLineMessageToUser(String id, Message message) {
		CompletableFuture<BotApiResponse> future = lineMessagingClient.pushMessage(new PushMessage(id, message));

		handleFuture(future);
	}

	private void handleExceptions(Exception e) {
		LogUtils.logThrowable(e);
		sendErrorLogToAdmins(e);
	}

	private void handleFuture(CompletableFuture<?> future) {
		try {
			future.get();
		} catch (InterruptedException | ExecutionException e) {
			handleExceptions(e);
			Thread.currentThread().interrupt();
		}
	}
}
