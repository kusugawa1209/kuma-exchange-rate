package com.kuma.exchange.core.helper.command;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.core.vo.FlexMessageStyles;
import com.kuma.exchange.enums.BankEnum;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.handler.ExchangeRateMessageHandler;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

@Component
public class ExchangeRateCommandHelper extends AbstractCommandHelper {

	@Autowired
	private ExchangeRateMessageHandler exchangeRateHandler;

	@Override
	public CommandEnum getSupportedCommand() {
		return CommandEnum.EXCHANGE_RATE;
	}

	@Override
	public void handleCommand(String senderId, String messageWithoutCommand) {
		Optional<FlexMessageStyles> stylesOptional = getThemesByUserId(senderId);
		if (StringUtils.equals(messageWithoutCommand, messageProperties.getHelpCommand())) {
			List<Box> commandExampleComponents = createCommandExampleComponents();

			FlexMessage message = flexMessageHelper.createFlexMessage(messageProperties.getRateExampleHeader(),
					messageProperties.getRateExampleHeader(), commandExampleComponents,
					messageProperties.getRateExampleFooters(), stylesOptional);
			botMessageService.pushMessage(senderId, message);
		} else if (StringUtils.isEmpty(messageWithoutCommand)) {
			pushDefaultRateInfo(senderId);
		} else {
			List<String> queryTokens = Lists
					.newArrayList(StringUtils.split(messageWithoutCommand, messageProperties.getSetupSplit())).stream()
					.filter(queryToken -> StringUtils.isNotBlank(queryToken)
							&& !StringUtils.containsAny(queryToken, "(", ")"))
					.collect(Collectors.toList());

			Set<BankEnum> queryBanks = Lists.newArrayList(BankEnum.values()).stream()
					.filter(bank -> queryTokens.stream()
							.anyMatch(queryToken -> StringUtils.containsIgnoreCase(bank.getDisplayName(), queryToken)))
					.collect(Collectors.toSet());

			Set<String> queryCurrencies = exchangeRateService.findExchangeRates().stream()
					.filter(rate -> queryTokens.stream().anyMatch(
							queryToken -> StringUtils.containsIgnoreCase(rate.getCurrencyDisplayName(), queryToken)))
					.map(ExchangeRate::getCurrencyCode).collect(Collectors.toSet());

			if (CollectionUtils.isEmpty(queryBanks) && CollectionUtils.isEmpty(queryCurrencies)) {
				List<Box> commandExampleComponents = createCommandExampleComponents();
				FlexMessage message = flexMessageHelper.createFlexMessage(messageProperties.getInvalidCommand(),
						messageProperties.getInvalidCommand(), commandExampleComponents,
						messageProperties.getRateExampleFooters(), stylesOptional);
				botMessageService.pushMessage(senderId, message);
			} else {
				List<ExchangeRate> rates = exchangeRateService.findExchangeRates().stream().filter(
						rate -> queryBanks.contains(rate.getBank()) || queryCurrencies.contains(rate.getCurrencyCode()))
						.sorted((x1, x2) -> Integer.compare(x1.getCurrency().getSorting(),
								x2.getCurrency().getSorting()))
						.collect(Collectors.toList());

				List<FlexMessage> rateMessages = exchangeRateHandler.convertToMessages(rates);

				botMessageService.pushMessages(senderId, Lists.newArrayList(rateMessages));
			}
		}
	}

	private List<Box> createCommandExampleComponents() {
		return messageProperties.getRateExamples().stream().map(example -> {
			MessageAction action = new MessageAction(example.getCommandSample(), example.getCommand());
			Button button = flexMessageHelper.createButton(action, Optional.empty());
			Text description = flexMessageHelper.createBodyText(example.getCommandDescription());

			return flexMessageHelper.createBox(Lists.newArrayList(button, description), FlexLayout.VERTICAL,
					FlexMarginSize.MD);
		}).collect(Collectors.toList());
	}

	private void pushDefaultRateInfo(String senderId) {
		List<ExchangeRate> rates = exchangeRateService.findFirstAvaliableRates();
		List<FlexMessage> rateMessages = exchangeRateHandler.convertToMessages(rates);

		botMessageService.pushMessages(senderId, Lists.newArrayList(rateMessages));
	}
}
