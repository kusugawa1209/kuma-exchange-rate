package com.kuma.exchange.core.service.impl;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.kuma.exchange.core.helper.command.CommandHelper;
import com.kuma.exchange.core.helper.command.DefaultCommandHelper;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.CommandService;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.enums.RoleEnum;
import com.kuma.exchange.properties.MessageProperties;

@Service
public class CommandServiceImpl implements CommandService {

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private FollowerService followerService;

	@Autowired
	private BotMessageServiceImpl botMessageService;

	@Autowired
	private DefaultCommandHelper defaultCommandHelper;

	@Autowired
	private MessageProperties commandMessages;

	@Override
	public void handleCommand(String followerId, String message) {
		final String trimedMessage = StringUtils.trim(message);
		Follower follower = followerService.getById(followerId);
		RoleEnum followerRole = follower.getRole();

		Map<String, CommandHelper> commandHelpers = applicationContext.getBeansOfType(CommandHelper.class);

		Optional<CommandHelper> commandHelperOptional = commandHelpers.entrySet().stream().filter(x -> {
			CommandHelper helper = x.getValue();
			CommandEnum supportedCommand = helper.getSupportedCommand();
			String supportedCommandText = supportedCommand.getCommandText();

			boolean isSupportedOperation = StringUtils.startsWithIgnoreCase(trimedMessage,
					commandMessages.getSetupPrefix() + supportedCommandText);
			boolean isFollowerHasCommandPermission = supportedCommand.getRoles().contains(followerRole);
			boolean isHelperHasCommandPermission = helper.findSupportedRoles().contains(followerRole);

			return isSupportedOperation && isFollowerHasCommandPermission && isHelperHasCommandPermission;
		}).map(Entry::getValue).findFirst();

		if (commandHelperOptional.isPresent()) {
			CommandHelper commandHelper = commandHelperOptional.get();

			String command = commandMessages.getSetupPrefix() + commandHelper.getSupportedCommand().getCommandText();
			String messageWithoutCommand = StringUtils.trim(StringUtils.removeStartIgnoreCase(trimedMessage, command));

			commandHelper.handleCommand(followerId, messageWithoutCommand);
		} else {
			botMessageService.pushMessage(followerId, defaultCommandHelper.getDefaultHelpMessage(followerId));
		}
	}
}
