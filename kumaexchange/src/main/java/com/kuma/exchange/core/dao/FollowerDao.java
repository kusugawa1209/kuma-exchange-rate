package com.kuma.exchange.core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.enums.RoleEnum;

@Repository
public interface FollowerDao extends JpaRepository<Follower, String> {

	@Modifying
	@Query("UPDATE Follower f SET f.displayName = ?2 WHERE f.id = ?1")
	public void updateDisplayName(String userId, String displayName);

	public List<Follower> findByDisplayName(String displayName);

	@Query("SELECT f FROM Follower f WHERE f.role = ?1")
	public List<Follower> findByRole(RoleEnum role);

	@Query("SELECT f FROM Follower f WHERE f.id = ?1 AND f.role = ?2")
	public Follower getByIdAndRole(String userId, RoleEnum role);

	@Modifying
	@Query("UPDATE Follower f SET f.themeId = ?2 WHERE f.id = ?1")
	public void updateTheme(String userId, Long themeId);
}
