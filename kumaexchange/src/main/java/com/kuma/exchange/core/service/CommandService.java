package com.kuma.exchange.core.service;

public interface CommandService {

	public void handleCommand(String followerId, String messageContent);
}