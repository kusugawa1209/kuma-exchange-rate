package com.kuma.exchange.core.service;

import java.util.List;

import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.enums.BankEnum;

public interface ExchangeRateService {

	void updateRates();

	List<ExchangeRate> findExchangeRates(BankEnum bank, String... currencies);

	List<ExchangeRate> findExchangeRates(String... currencies);
	
	List<ExchangeRate> findExchangeRates();
	
	List<ExchangeRate> findFirstAvaliableRates();
}