package com.kuma.exchange.core.vo;

import java.nio.file.Path;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class DownloadedContent {

	private Path path;

	private String uri;
}
