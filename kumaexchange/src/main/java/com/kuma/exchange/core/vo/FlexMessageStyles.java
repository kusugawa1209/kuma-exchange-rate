package com.kuma.exchange.core.vo;

import lombok.Data;

@Data
public class FlexMessageStyles {

	private Long id;

	private String name;

	private String headerTextColor;

	private String headerDescriptionColor;

	private String buttonColor;

	private String bubbleBackgroundColor;

	private String bubbleSeparatorColor;

	private String imageBackgroundColor;
}
