package com.kuma.exchange.core.helper.command;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.exchange.core.helper.message.FlexMessageHelper;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.enums.RoleEnum;
import com.kuma.exchange.properties.MessageProperties;
import com.kuma.exchange.web.view.CommandPostBackView;
import com.kuma.utils.JsonUtils;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.FlexComponent;

@Component
public class DefaultCommandHelper {

	@Autowired
	private MessageProperties messageProperties;

	@Autowired
	private FollowerService followerService;

	@Autowired
	private FlexMessageHelper flexMessageHelper;

	public FlexMessage getDefaultHelpMessage(String senderId) {
		Follower follower = followerService.getById(senderId);
		RoleEnum role = follower.getRole();

		List<FlexComponent> components = CommandEnum.findCommandsByRole(role).stream().filter(CommandEnum::isShowInMenu)
				.map((CommandEnum command) -> {
					String commandName = command.getCommandText();
					CommandPostBackView postBackData = new CommandPostBackView();
					postBackData.setParams(command);
					PostbackAction action = new PostbackAction(commandName, JsonUtils.toJsonString(postBackData));

					return flexMessageHelper.createButton(action, Optional.empty());
				}).collect(Collectors.toList());

		return flexMessageHelper.createFlexMessage(messageProperties.getSupportedCommands(),
				messageProperties.getInvalidCommand(), components, messageProperties.getSupportedCommandsFooter(),
				Optional.empty());
	}
}
