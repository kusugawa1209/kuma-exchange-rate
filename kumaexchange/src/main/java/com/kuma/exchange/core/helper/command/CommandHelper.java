package com.kuma.exchange.core.helper.command;

import java.util.Set;

import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.enums.RoleEnum;

public interface CommandHelper {

	public Set<RoleEnum> findSupportedRoles();

	public CommandEnum getSupportedCommand();

	public void handleCommand(String senderId, String messageWithoutCommand);
}
