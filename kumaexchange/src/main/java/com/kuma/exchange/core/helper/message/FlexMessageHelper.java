package com.kuma.exchange.core.helper.message;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.core.vo.FlexMessageStyles;
import com.kuma.exchange.properties.ThemeProperties;
import com.linecorp.bot.model.action.Action;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Image;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectMode;
import com.linecorp.bot.model.message.flex.component.Image.ImageAspectRatio;
import com.linecorp.bot.model.message.flex.component.Image.ImageSize;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.container.BubbleStyles;
import com.linecorp.bot.model.message.flex.container.BubbleStyles.BlockStyle;
import com.linecorp.bot.model.message.flex.container.Carousel;
import com.linecorp.bot.model.message.flex.container.FlexContainer;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexDirection;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexGravity;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;

@Component
public class FlexMessageHelper {

	@Autowired
	private FollowerService followerService;

	@Autowired
	private ThemeProperties themeProperties;

	private FlexMessageStyles defaultStyles;

	@PostConstruct
	public void init() {
		FlexMessageStyles defaultTheme = themeProperties.getDefaultStyle();
		this.defaultStyles = defaultTheme;
	}

	private static final int MAX_BUBBLE_SIZE = 10;

	private static final String DEFAULT_FONT_COLOR = "#000000";

	public <F extends FlexComponent> FlexMessage createFlexMessage(String altText, String headerText, List<F> contents,
			String footerText, Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, stylesOptional);

		Box body = createBox(contents, FlexLayout.VERTICAL);

		Box footer = createFooter(footerText);

		Bubble bubble = createBubble(header, null, body, footer, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public Box createFooter(String footerText) {
		Text footerComponent = createFooterText(footerText);
		return createBox(Lists.newArrayList(footerComponent), FlexLayout.VERTICAL);
	}

	public <F extends FlexComponent> FlexMessage createFlexMessage(String altText, String headerText, List<F> contents,
			Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, stylesOptional);

		Box body = createBox(contents, FlexLayout.VERTICAL);

		Bubble bubble = createBubble(header, null, body, null, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public <F extends FlexComponent> FlexMessage createFlexMessage(String altText, String headerText, List<F> contents,
			List<String> footerTexts, Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, stylesOptional);

		Box body = createBox(contents, FlexLayout.VERTICAL);

		Box footer = createFooter(footerTexts);

		Bubble bubble = createBubble(header, null, body, footer, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public Box createFooter(List<String> footerTexts) {
		List<Text> footerComponents = createFooterTexts(footerTexts);
		return createBox(footerComponents, FlexLayout.VERTICAL);
	}

	public FlexMessage createFlexMessage(String altText, String headerText, String bodyText,
			Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, stylesOptional);

		Box body = createBody(bodyText);

		Bubble bubble = createBubble(header, null, body, null, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public Box createBody(String bodyText) {
		Text bodyTextComponent = createBodyText(bodyText);
		return createBox(Lists.newArrayList(bodyTextComponent), FlexLayout.VERTICAL);
	}

	public Box createHeader(String headerText, Optional<FlexMessageStyles> stylesOptional) {
		Text headerComponent = createHeaderText(headerText, stylesOptional);
		return createBox(Lists.newArrayList(headerComponent), FlexLayout.VERTICAL);
	}

	public Box createHeader(String headerText, String headerDescriptionText,
			Optional<FlexMessageStyles> stylesOptional) {
		Text headerTextComponent = createHeaderText(headerText, stylesOptional);
		Text headerDescriptionTextComponent = createHeaderDescriptionText(headerDescriptionText, stylesOptional);

		return createBox(Lists.newArrayList(headerTextComponent, headerDescriptionTextComponent), FlexLayout.VERTICAL);
	}

	public FlexMessage createFlexMessageWithTexts(String altText, String headerText, List<String> bodyTexts,
			Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, stylesOptional);

		Box body = createBody(bodyTexts);

		Bubble bubble = createBubble(header, null, body, null, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public FlexMessage createFlexMessageWithTexts(String altText, String headerText, String headerDescription,
			List<String> bodyTexts, Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, headerDescription, stylesOptional);

		Box body = createBody(bodyTexts);

		Bubble bubble = createBubble(header, null, body, null, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public Box createBody(List<String> bodyTexts) {
		List<Text> bodyTextComponents = bodyTexts.stream().map(this::createBodyText).collect(Collectors.toList());
		return createBox(bodyTextComponents, FlexLayout.VERTICAL);
	}

	public FlexMessage createFlexMessageWithTexts(String altText, String headerText, List<String> bodyTexts,
			String footerText, Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, stylesOptional);

		Box body = createBody(bodyTexts);

		Box footer = createFooter(footerText);

		Bubble bubble = createBubble(header, null, body, footer, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public FlexMessage createFlexMessageWithTexts(String altText, String headerText, List<String> bodyTexts,
			List<String> footerTexts, Optional<FlexMessageStyles> stylesOptional) {
		Box header = createHeader(headerText, stylesOptional);

		Box body = createBody(bodyTexts);

		Box footer = createFooter(footerTexts);

		Bubble bubble = createBubble(header, null, body, footer, stylesOptional);

		return createFlexMessage(altText, bubble);
	}

	public Text createHeaderText(String text, Optional<FlexMessageStyles> stylesOptional) {
		FlexMessageStyles styles = stylesOptional.orElse(defaultStyles);
		return new Text(1, text, FlexFontSize.XL, FlexAlign.START, FlexGravity.CENTER, styles.getHeaderTextColor(),
				TextWeight.REGULAR, true, FlexMarginSize.NONE, null);
	}

	public Text createHeaderDescriptionText(String text, Optional<FlexMessageStyles> stylesOptional) {
		FlexMessageStyles styles = stylesOptional.orElse(defaultStyles);
		return new Text(1, text, FlexFontSize.SM, FlexAlign.START, FlexGravity.CENTER,
				styles.getHeaderDescriptionColor(), TextWeight.REGULAR, true, FlexMarginSize.NONE, null);
	}

	public Text createBodyText(String text) {
		return new Text(1, text, FlexFontSize.Md, FlexAlign.START, FlexGravity.CENTER, DEFAULT_FONT_COLOR,
				TextWeight.REGULAR, true, FlexMarginSize.NONE, null);
	}

	public List<Text> createBodyTexts(List<String> bodyTexts) {
		return bodyTexts.stream().map(this::createBodyText).collect(Collectors.toList());
	}

	public Text createFooterText(String text) {
		return new Text(1, text, FlexFontSize.Md, FlexAlign.START, FlexGravity.CENTER, DEFAULT_FONT_COLOR,
				TextWeight.REGULAR, true, FlexMarginSize.NONE, null);
	}

	public List<Text> createFooterTexts(List<String> footerTexts) {
		return footerTexts.stream().map(this::createFooterText).collect(Collectors.toList());
	}

	public Button createButton(Action action, Optional<FlexMessageStyles> stylesOptional) {
		FlexMessageStyles styles = stylesOptional.orElse(defaultStyles);
		return new Button(1, styles.getButtonColor(), ButtonStyle.PRIMARY, action, FlexGravity.CENTER,
				FlexMarginSize.SM, ButtonHeight.SMALL);
	}

	public <F extends FlexComponent> Box createBox(List<F> contents, FlexLayout flexLayout) {
		return createBox(contents, flexLayout, FlexMarginSize.NONE);
	}

	public <F extends FlexComponent> Box createBox(List<F> contents, FlexLayout flexLayout, FlexMarginSize margin) {
		List<FlexComponent> components = Lists.newArrayList(contents);
		return new Box(flexLayout, 0, components, FlexMarginSize.SM, margin);
	}

	public Bubble createBubble(Box header, Image hero, Box body, Box footer,
			Optional<FlexMessageStyles> stylesOptional) {
		FlexMessageStyles styles = stylesOptional.orElse(defaultStyles);
		BlockStyle blockStyle = new BlockStyle(styles.getBubbleBackgroundColor(), true,
				styles.getBubbleSeparatorColor());
		BubbleStyles bubbleStyles = new BubbleStyles(blockStyle, blockStyle, blockStyle, blockStyle);

		return new Bubble(FlexDirection.LTR, bubbleStyles, header, hero, body, footer);
	}

	public Image createImage(String url, Optional<FlexMessageStyles> stylesOptional) {
		FlexMessageStyles styles = stylesOptional.orElse(defaultStyles);
		return new Image(1, url, ImageSize.XXL, ImageAspectRatio.R1TO1, ImageAspectMode.Fit,
				styles.getImageBackgroundColor(), FlexAlign.CENTER, null, FlexGravity.CENTER, FlexMarginSize.DEFAULT);
	}

	public List<FlexMessage> createFlexMessages(String altText, List<Bubble> bubbles) {
		return Lists.partition(bubbles, MAX_BUBBLE_SIZE).stream().map(partedBubbles -> {
			Carousel contents = new Carousel(partedBubbles);
			return new FlexMessage(altText, contents);
		}).collect(Collectors.toList());
	}

	public FlexMessage createBubbleFlexMessage(String altText, Bubble bubble) {
		return new FlexMessage(altText, bubble);
	}

	public FlexMessage createCarouselFlexMessage(String altText, Carousel carousel) {
		return new FlexMessage(altText, carousel);
	}

	public FlexMessage createFlexMessage(String altText, FlexContainer container) {
		if (container instanceof Bubble) {
			return createBubbleFlexMessage(altText, (Bubble) container);
		} else if (container instanceof Carousel) {
			return createCarouselFlexMessage(altText, (Carousel) container);
		}

		throw new IllegalArgumentException("Not supported container type: " + container.getClass().getSimpleName());

	}

	public Message convertToThemedMessage(String userId, Message message) {
		Long themeId = followerService.getById(userId).getThemeId();
		Optional<FlexMessageStyles> stylesOptional = themeProperties.getById(themeId);

		return convertToThemedMessage(message, stylesOptional);

	}

	private Message convertToThemedMessage(Message message, Optional<FlexMessageStyles> stylesOptional) {
		if (!(message instanceof FlexMessage)) {
			return message;
		}
		FlexMessage flexMessage = (FlexMessage) message;
		FlexContainer contents = flexMessage.getContents();
		return createFlexMessage(flexMessage.getAltText(), appendTheme(contents, stylesOptional.orElse(defaultStyles)));
	}

	public List<Message> convertToThemedMessages(String userId, List<Message> messages) {
		Long themeId = followerService.getById(userId).getThemeId();
		Optional<FlexMessageStyles> stylesOptional = themeProperties.getById(themeId);

		return messages.stream().map(message -> convertToThemedMessage(message, stylesOptional))
				.collect(Collectors.toList());
	}

	public Map<String, Message> convertToThemedMessages(Collection<String> userIds, Message message) {
		return followerService.findAll().stream().filter(follower -> userIds.contains(follower.getId()))
				.collect(Collectors.toMap(Follower::getId, follower -> {
					Optional<FlexMessageStyles> stylesOptional = themeProperties.getById(follower.getThemeId());
					return convertToThemedMessage(message, stylesOptional);
				}));
	}

	public Map<String, List<Message>> convertToThemedMessages(Collection<String> userIds, List<Message> messages) {
		return followerService.findAll().stream().filter(follower -> userIds.contains(follower.getId()))
				.collect(Collectors.toMap(Follower::getId, follower -> {
					Optional<FlexMessageStyles> stylesOptional = themeProperties.getById(follower.getThemeId());

					return messages.stream().map(message -> convertToThemedMessage(message, stylesOptional))
							.collect(Collectors.toList());
				}));
	}

	private Image appendImageTheme(Image component, FlexMessageStyles styles) {
		return createImage(component.getUrl(), Optional.of(styles));
	}

	private Button appendButtonTheme(Button button, FlexMessageStyles styles) {
		return createButton(button.getAction(), Optional.of(styles));
	}

	private Box appendBoxTheme(Box component, FlexMessageStyles styles) {
		List<FlexComponent> contents = component.getContents().stream()
				.map(source -> (FlexComponent) appendTheme(source, styles)).collect(Collectors.toList());

		return createBox(contents, component.getLayout(), component.getMargin());
	}

	@SuppressWarnings("unchecked")
	private <F extends FlexComponent> F appendTheme(FlexComponent component, FlexMessageStyles styles) {
		if (component == null) {
			return null;
		}

		if (component instanceof Text) {
			return (F) component;
		} else if (component instanceof Image) {
			return (F) appendImageTheme((Image) component, styles);
		} else if (component instanceof Button) {
			return (F) appendButtonTheme((Button) component, styles);
		} else if (component instanceof Box) {
			return (F) appendBoxTheme((Box) component, styles);
		}

		throw new IllegalArgumentException("Not supported instance type: " + component.getClass().getSimpleName());
	}

	private Bubble appendBubbleTheme(Bubble bubble, FlexMessageStyles styles) {
		Box header = bubble.getHeader();
		List<Text> themedHeaders = header.getContents().stream()
				.map(content -> createHeaderDescriptionText(((Text) content).getText(), Optional.of(styles)))
				.collect(Collectors.toList());
		Text headerTitle = themedHeaders.get(0);
		Text themedHeaderTitle = createHeaderText(headerTitle.getText(), Optional.of(styles));
		themedHeaders.set(0, themedHeaderTitle);
		Box themedHeader = createBox(themedHeaders, FlexLayout.VERTICAL);
		return createBubble(themedHeader, bubble.getHero(), appendTheme(bubble.getBody(), styles),
				appendTheme(bubble.getFooter(), styles), Optional.of(styles));

	}

	private Carousel appendCarouselTheme(Carousel carousel, FlexMessageStyles styles) {
		List<Bubble> newContents = carousel.getContents().stream().map(bubble -> appendBubbleTheme(bubble, styles))
				.collect(Collectors.toList());
		return new Carousel(newContents);
	}

	@SuppressWarnings("unchecked")
	private <F extends FlexContainer> F appendTheme(FlexContainer container, FlexMessageStyles styles) {
		if (container == null) {
			return null;
		}

		if (container instanceof Bubble) {
			return (F) appendBubbleTheme((Bubble) container, styles);
		} else if (container instanceof Carousel) {
			return (F) appendCarouselTheme((Carousel) container, styles);
		}

		throw new IllegalArgumentException("Not supported instance type: " + container.getClass().getSimpleName());
	}
}
