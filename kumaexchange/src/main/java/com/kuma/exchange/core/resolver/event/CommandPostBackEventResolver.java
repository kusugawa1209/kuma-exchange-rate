package com.kuma.exchange.core.resolver.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.exchange.core.service.CommandService;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.enums.PostBackTypeEnum;
import com.kuma.exchange.properties.MessageProperties;
import com.kuma.exchange.web.view.CommandPostBackView;

@Component
public class CommandPostBackEventResolver extends AbstractPostBackEventResolver<CommandPostBackView> {

	@Autowired
	private CommandService commandService;

	@Autowired
	private MessageProperties commandMessages;

	@Override
	public PostBackTypeEnum getSupportedPostBackEventType() {
		return PostBackTypeEnum.COMMAND;
	}

	@Override
	protected Class<CommandPostBackView> getEventDataViewClass() {
		return CommandPostBackView.class;
	}

	@Override
	protected void handleEvent(String senderId, CommandPostBackView paramsView) {
		CommandEnum command = paramsView.getParams();
		commandService.handleCommand(senderId, commandMessages.getSetupPrefix() + command.getCommandText());
	}

}
