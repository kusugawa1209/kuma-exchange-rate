package com.kuma.exchange.core.service;

import com.linecorp.bot.model.event.PostbackEvent;

public interface PostBackEventService {

	public void handleEvent(PostbackEvent event);
}