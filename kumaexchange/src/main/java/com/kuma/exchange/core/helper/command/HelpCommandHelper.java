package com.kuma.exchange.core.helper.command;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.vo.FlexMessageStyles;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.enums.RoleEnum;
import com.kuma.exchange.web.view.CommandPostBackView;
import com.kuma.utils.JsonUtils;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.FlexComponent;

@Component
public class HelpCommandHelper extends AbstractCommandHelper {

	@Override
	public CommandEnum getSupportedCommand() {
		return CommandEnum.HELP;
	}

	@Override
	public void handleCommand(String senderId, String messageWithoutCommand) {
		Optional<FlexMessageStyles> stylesOptional = getThemesByUserId(senderId);
		Follower sender = followerService.getById(senderId);
		RoleEnum role = sender.getRole();

		List<FlexComponent> components = CommandEnum.findCommandsByRole(role).stream().filter(CommandEnum::isShowInMenu)
				.map((CommandEnum command) -> {
					String commandName = command.getCommandText();
					CommandPostBackView postBackData = new CommandPostBackView();
					postBackData.setParams(command);
					PostbackAction action = new PostbackAction(commandName, JsonUtils.toJsonString(postBackData));

					return flexMessageHelper.createButton(action, stylesOptional);
				}).collect(Collectors.toList());

		FlexMessage message = flexMessageHelper.createFlexMessage(messageProperties.getSupportedCommands(),
				messageProperties.getSupportedCommands(), components, messageProperties.getSupportedCommandsFooter(),
				Optional.empty());

		botMessageService.pushMessage(senderId, message);
	}
}
