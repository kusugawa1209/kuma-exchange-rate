package com.kuma.exchange.core.retriver.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.enums.BankEnum;

@Service
@Component
@Transactional
public class BotBankRateRetriver extends AbstractJSoupRateRetriver {
	@Override
	public BankEnum getBank() {
		return BankEnum.BOT_BANK;
	}

	@Override
	protected List<ExchangeRate> tryGetExchangeRates(Document document) throws IOException {
		List<ExchangeRate> rates = Lists.newArrayList();

		Elements trs = document.select("table[title=牌告匯率]>tbody>tr");

		for (Element tr : trs) {
			Elements tds = tr.children();
			ExchangeRate rate = new ExchangeRate();
			for (Element td : tds) {
				String tdText = td.ownText();
				String dataTableValue = td.attr("data-table");

				switch (dataTableValue) {
				case "幣別":
					String currency = td.child(0).child(2).ownText();
					rate.setCurrency(convertToCurrency(currency));
					break;
				case "本行現金買入":
					rate.setCashBuy(NumberUtils.toDouble(tdText));
					break;
				case "本行現金賣出":
					rate.setCashSell(NumberUtils.toDouble(tdText));
					break;
				case "本行即期買入":
					rate.setSpotBuy(NumberUtils.toDouble(tdText));
					break;
				case "本行即期賣出":
					rate.setSpotSell(NumberUtils.toDouble(tdText));
					break;
				default:
					break;
				}
			}
			rate.setCreationDate(new Date());

			rate.setBank(getBank());
			rates.add(rate);
		}

		return rates;
	}
}
