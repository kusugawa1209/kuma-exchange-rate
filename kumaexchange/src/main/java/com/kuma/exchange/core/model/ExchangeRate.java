package com.kuma.exchange.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.kuma.exchange.enums.BankEnum;
import com.kuma.exchange.enums.CurrencyEnum;

import lombok.Data;

@Data
@Entity
@Table(name = "EXCHANGE_RATE")
public class ExchangeRate {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "CURRENCY")
	private CurrencyEnum currency;

	@Column(name = "CASH_BUY")
	private Double cashBuy;

	@Column(name = "CASH_SELL")
	private Double cashSell;

	@Column(name = "SPOT_BUY")
	private Double spotBuy;

	@Column(name = "SPOT_SELL")
	private Double spotSell;

	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "BANK")
	private BankEnum bank;

	/**
	 * @return 幣別顯示名稱, 如: 美金
	 */
	public String getDisplayName() {
		return this.currency.getDisplayName();
	}

	/**
	 * @return 幣別代碼, 如: USD
	 */
	public String getCurrencyCode() {
		return this.currency.name();
	}

	/**
	 * @return 幣別完整名稱, 如: 美金 (USD)
	 */
	public String getCurrencyDisplayName() {
		return this.currency.getFullName();
	}

	/**
	 * @return 匯率完整名稱, 如: 臺灣銀行 - 美金 (USD)
	 */
	public String getRateFullDisplayName() {
		return String.format("%s - %s", getBank().getDisplayName(), getCurrencyDisplayName());
	}
}
