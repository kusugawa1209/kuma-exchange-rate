package com.kuma.exchange.core.service;

import java.util.Collection;
import java.util.List;

import com.kuma.exchange.core.model.HistoryRate;

public interface HistoryRateService {

	public List<HistoryRate> saveHistoryRates(Collection<HistoryRate> historyRates);
}