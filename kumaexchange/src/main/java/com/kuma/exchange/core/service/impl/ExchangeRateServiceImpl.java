package com.kuma.exchange.core.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.dao.ExchangeRateDao;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.core.model.HistoryRate;
import com.kuma.exchange.core.retriver.RateRetriver;
import com.kuma.exchange.core.service.ExchangeRateService;
import com.kuma.exchange.core.service.HistoryRateService;
import com.kuma.exchange.enums.BankEnum;
import com.kuma.exchange.enums.CurrencyEnum;
import com.kuma.exchange.utils.ApplicationContextUtils;

@Service
@Transactional
public class ExchangeRateServiceImpl implements ExchangeRateService {

	private static final List<CurrencyEnum> DEFAULT_DISPLAY_CURRENCIES = Lists.newArrayList(CurrencyEnum.USD,
			CurrencyEnum.JPY, CurrencyEnum.AUD, CurrencyEnum.EUR, CurrencyEnum.CNY);

	@Autowired
	private HistoryRateService historyRateService;

	@Autowired
	private ExchangeRateDao exchangeRateDao;

	@Override
	public void updateRates() {
		Map<String, RateRetriver> retrivers = ApplicationContextUtils.getApplicationContext()
				.getBeansOfType(RateRetriver.class);
		Set<Entry<String, RateRetriver>> retriverEntries = retrivers.entrySet();

		List<ExchangeRate> exchangeRates = retriverEntries.stream().map(x -> x.getValue().retriveRates())
				.flatMap(Collection::stream).collect(Collectors.toList());

		moveCurrentRatesIntoHistory();

		exchangeRateDao.save(exchangeRates);
	}

	@Override
	public List<ExchangeRate> findExchangeRates(BankEnum bank, String... currencies) {
		List<CurrencyEnum> displayCurrencies = prepareDisplayCurrencies(currencies);

		List<ExchangeRate> rates = exchangeRateDao.findByBank(bank);
		if (CollectionUtils.isEmpty(rates)) {
			rates = findFirstAvaliableRates();
		}
		return filterAndSortExchangeRatesByCurrencies(displayCurrencies, rates);
	}

	@Override
	public List<ExchangeRate> findExchangeRates(String... currencies) {
		List<CurrencyEnum> displayCurrencies = prepareDisplayCurrencies(currencies);

		List<ExchangeRate> rates = exchangeRateDao.findAll();
		if (CollectionUtils.isEmpty(rates)) {
			rates = findFirstAvaliableRates();
		}

		return filterAndSortExchangeRatesByCurrencies(displayCurrencies, rates);
	}

	@Override
	public List<ExchangeRate> findExchangeRates() {
		return exchangeRateDao.findAll();
	}

	@Override
	public List<ExchangeRate> findFirstAvaliableRates() {
		List<ExchangeRate> rates = exchangeRateDao.findAll();
		List<CurrencyEnum> displayCurrencies = prepareDisplayCurrencies();
		Map<BankEnum, List<ExchangeRate>> groupedRates = rates.stream()
				.collect(Collectors.groupingBy(ExchangeRate::getBank));

		Optional<List<ExchangeRate>> firstAvaliableRates = groupedRates.entrySet().stream()
				.sorted((entry1, entry2) -> Integer.compare(entry1.getKey().getOrder(), entry2.getKey().getOrder()))
				.map(Entry::getValue).filter(CollectionUtils::isNotEmpty).findFirst();

		if (firstAvaliableRates.isPresent()) {
			List<ExchangeRate> foundRates = firstAvaliableRates.get();

			return foundRates.stream().filter(rate -> {
				return displayCurrencies.contains(rate.getCurrency());
			}).sorted((rate1, rate2) -> {
				return Integer.compare(rate1.getCurrency().getSorting(), rate2.getCurrency().getSorting());
			}).collect(Collectors.toList());
		} else {
			return Lists.newArrayList();
		}
	}

	private List<CurrencyEnum> prepareDisplayCurrencies(String... currencies) {
		List<CurrencyEnum> displayCurrencies;
		if (currencies == null || currencies.length == 0) {
			displayCurrencies = DEFAULT_DISPLAY_CURRENCIES;
		} else {
			displayCurrencies = CurrencyEnum.findByNamesOrDisplayNames(currencies);
		}
		return displayCurrencies;
	}

	private List<ExchangeRate> filterAndSortExchangeRatesByCurrencies(List<CurrencyEnum> displayCurrencies,
			List<ExchangeRate> rates) {
		Stream<ExchangeRate> displayRateStream = rates.stream()
				.filter(rate -> displayCurrencies.contains(rate.getCurrency()));
		return displayRateStream
				.sorted((x1, x2) -> Integer.compare(x1.getCurrency().getSorting(), x2.getCurrency().getSorting()))
				.collect(Collectors.toList());
	}

	private void moveCurrentRatesIntoHistory() {
		List<ExchangeRate> exchangeRates = exchangeRateDao.findAll();

		List<HistoryRate> historyRates = exchangeRates.stream().map(source -> {
			HistoryRate target = new HistoryRate();

			target.setBank(source.getBank());
			target.setCashBuy(source.getCashBuy());
			target.setCashSell(source.getCashSell());
			target.setCreationDate(source.getCreationDate());
			target.setCurrency(source.getCurrency());
			target.setSpotBuy(source.getSpotBuy());
			target.setSpotSell(source.getSpotSell());

			return target;
		}).collect(Collectors.toList());

		historyRateService.saveHistoryRates(historyRates);

		exchangeRateDao.deleteAllInBatch();
	}
}
