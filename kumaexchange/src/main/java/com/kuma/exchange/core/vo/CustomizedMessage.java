package com.kuma.exchange.core.vo;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.linecorp.bot.model.message.Message;

import lombok.Data;

@Data
public class CustomizedMessage {

	@NonNull
	private String receiverId;

	@NonNull
	private Message message;
}
