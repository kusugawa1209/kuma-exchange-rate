package com.kuma.exchange.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.kuma.exchange.core.model.HistoryRate;

@Repository
public interface HistoryRateDao extends JpaRepository<HistoryRate, Long>, JpaSpecificationExecutor<HistoryRate> {
}
