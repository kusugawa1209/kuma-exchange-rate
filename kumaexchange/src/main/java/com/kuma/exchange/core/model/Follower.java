package com.kuma.exchange.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.kuma.exchange.enums.RoleEnum;

import lombok.Data;

@Data
@Entity
@Table(name = "FOLLOWER")
public class Follower {

	@Id
	@Column(name = "ID")
	private String id;

	@Column(name = "DISPLAY_NAME")
	private String displayName;

	@Enumerated(EnumType.STRING)
	@Column(name = "ROLE")
	private RoleEnum role;

	@Column(name = "THEME_ID")
	private Long themeId;
}
