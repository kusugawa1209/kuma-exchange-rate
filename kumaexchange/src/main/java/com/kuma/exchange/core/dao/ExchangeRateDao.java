package com.kuma.exchange.core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.enums.BankEnum;

@Repository
public interface ExchangeRateDao extends JpaRepository<ExchangeRate, Long>, JpaSpecificationExecutor<ExchangeRate> {

	public List<ExchangeRate> findByBank(BankEnum bank);
}
