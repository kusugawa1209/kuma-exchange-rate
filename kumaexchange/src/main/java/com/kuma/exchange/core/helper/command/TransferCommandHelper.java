package com.kuma.exchange.core.helper.command;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.enums.CommandEnum;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class TransferCommandHelper extends AbstractCommandHelper {

	@Override
	public CommandEnum getSupportedCommand() {
		return CommandEnum.TRANSFER;
	}

	@Override
	public void handleCommand(String senderId, String messageWithoutCommand) {

		boolean isSenderAdmin = followerService.isAdmin(senderId);

		if (isSenderAdmin) {
			handleAdminCommand(senderId, messageWithoutCommand);
		} else {
			handleUserCommand(senderId, messageWithoutCommand);
		}
	}

	private void handleAdminCommand(String senderId, String messageWithoutCommand) {
		List<Follower> followers = followerService.findAll();

		Optional<Follower> followerOptional = followers.stream()
				.filter(x -> StringUtils.startsWithIgnoreCase(messageWithoutCommand, x.getDisplayName())).findAny();
		if (followerOptional.isPresent()) {
			Follower follower = followerOptional.get();
			String followerDisplayName = follower.getDisplayName();

			String messageFromAdminBody = StringUtils.removeStartIgnoreCase(messageWithoutCommand, followerDisplayName);
			messageFromAdminBody = StringUtils.trim(messageFromAdminBody);

			Follower admin = followerService.getById(senderId);
			String messageFromAdminHeader = String.format(messageProperties.getMessagesFromUserPattern(),
					admin.getDisplayName());

			FlexMessage messageFromAdmin = flexMessageHelper.createFlexMessage(messageFromAdminHeader,
					messageFromAdminHeader, messageFromAdminBody, Optional.empty());

			botMessageService.pushMessage(follower.getId(), messageFromAdmin);

			String headerText = messageProperties.getMessageTransferSuccess();

			String contentText = String.format(messageProperties.getTransferToUserSuccessPattern(),
					messageFromAdminBody, followerDisplayName);

			FlexMessage messageToUser = flexMessageHelper.createFlexMessage(headerText, headerText, contentText,
					Optional.empty());

			botMessageService.pushMessage(senderId, messageToUser);
		} else {
			FlexMessage message = flexMessageHelper.createFlexMessage(messageProperties.getInvalidCommand(),
					messageProperties.getInvalidCommand(), messageProperties.getTransferAdminExample(),
					Optional.empty());

			botMessageService.pushMessage(senderId, message);
		}
	}

	private void handleUserCommand(String senderId, String messageWithoutCommand) {
		Follower sender = followerService.getById(senderId);

		if (StringUtils.isBlank(messageWithoutCommand)) {
			FlexMessage message = flexMessageHelper.createFlexMessage(messageProperties.getInvalidCommand(),
					messageProperties.getInvalidCommand(), messageProperties.getTransferUserExample(),
					Optional.empty());

			botMessageService.pushMessage(senderId, message);
		} else {

			String messageToAdminHeaderText = String.format(messageProperties.getMessagesFromUserPattern(),
					sender.getDisplayName());

			FlexMessage messageToAdmin = flexMessageHelper.createFlexMessage(messageToAdminHeaderText,
					messageToAdminHeaderText, messageWithoutCommand, Optional.empty());
			botMessageService.pushMessageToAdmins(messageToAdmin);

			String messageToUserHeaderText = messageProperties.getMessageTransferSuccess();
			String messageToUserContentText = String.format(messageProperties.getTransferToUserSuccessPattern(),
					messageWithoutCommand, sender.getDisplayName());
			FlexMessage messageToUser = flexMessageHelper.createFlexMessage(messageToUserHeaderText,
					messageToUserHeaderText, messageToUserContentText, Optional.empty());
			botMessageService.pushMessage(senderId, messageToUser);
		}
	}
}
