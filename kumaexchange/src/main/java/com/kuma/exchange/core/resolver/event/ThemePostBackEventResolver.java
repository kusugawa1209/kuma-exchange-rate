package com.kuma.exchange.core.resolver.event;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.service.BotMessageService;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.core.vo.FlexMessageStyles;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.enums.PostBackTypeEnum;
import com.kuma.exchange.web.view.CommandPostBackView;
import com.kuma.exchange.web.view.ThemeChoiceView;
import com.kuma.exchange.web.view.ThemePostBackView;
import com.kuma.utils.JsonUtils;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;

@Component
public class ThemePostBackEventResolver extends AbstractPostBackEventResolver<ThemePostBackView> {

	@Autowired
	private FollowerService followerService;

	@Autowired
	private BotMessageService botMessageService;

	@Override
	public PostBackTypeEnum getSupportedPostBackEventType() {
		return PostBackTypeEnum.THEME;
	}

	@Override
	protected Class<ThemePostBackView> getEventDataViewClass() {
		return ThemePostBackView.class;
	}

	@Override
	protected void handleEvent(String senderId, ThemePostBackView paramsView) {
		ThemeChoiceView themeChoice = paramsView.getParams();
		if (themeChoice.isChoose()) {
			followerService.updateTheme(senderId, themeChoice.getId());

			String themeName = themeProperties.getById(themeChoice.getId()).orElse(themeProperties.getDefaultStyle())
					.getName();
			String updatedBodyText = String.format(messageProperties.getThemedChangedPattern(), themeName);
			
			Optional<FlexMessageStyles> stylesOptional = Optional.of(themeProperties.getDefaultStyle());
			Box header = flexMessageHelper.createHeader(messageProperties.getThemeUpdated(), stylesOptional);
			Box body = flexMessageHelper.createBody(updatedBodyText);

			CommandEnum command = CommandEnum.THEME;
			CommandPostBackView postBackData = new CommandPostBackView();
			postBackData.setParams(command);
			PostbackAction action = new PostbackAction(messageProperties.getChangeOtherTheme(),
					JsonUtils.toJsonString(postBackData));
			Button button = flexMessageHelper.createButton(action, stylesOptional);

			Box footer = flexMessageHelper.createBox(Lists.newArrayList(button), FlexLayout.VERTICAL);

			Bubble bubble = flexMessageHelper.createBubble(header, null, body, footer, stylesOptional);
			FlexMessage message = flexMessageHelper.createBubbleFlexMessage(messageProperties.getThemeUpdated(),
					bubble);

			botMessageService.pushMessage(senderId, message);
		}
	}
}
