package com.kuma.exchange.core.helper.command;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.utils.FurtureUtils;
import com.kuma.utils.LogUtils;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;

@Component
public class FollowersCommandHelper extends AbstractCommandHelper {

	private static final String FOLLOWER_MESSAGE_PATTERN = "%s : %s";

	@Override
	public CommandEnum getSupportedCommand() {
		return CommandEnum.FOLLOWERS;
	}

	@Override
	public void handleCommand(String senderId, String messageWithoutCommand) {
		updateFollowerDisplayNames();
		sendFollowerInfosToAdmins();
	}

	private void sendFollowerInfosToAdmins() {
		List<Follower> followers = followerService.findAll();

		List<String> followerInfos = followers.stream().map((Follower follower) -> {
			String followerId = follower.getId();
			String followerDisplayName = follower.getDisplayName();

			return String.format(FOLLOWER_MESSAGE_PATTERN, followerDisplayName, followerId);

		}).collect(Collectors.toList());

		String followersCountText = String.format(messageProperties.getTotalUsersCountPattern(), followers.size());

		FlexMessage message = flexMessageHelper.createFlexMessageWithTexts(messageProperties.getUsers(),
				messageProperties.getUsers(), followersCountText, followerInfos,
				Optional.of(themeProperties.getDefaultStyle()));

		botMessageService.pushMessageToAdmins(message);
	}
	
	private void updateFollowerDisplayNames() {
		List<Follower> followers = followerService.findAll();
		Stream<CompletableFuture<UserProfileResponse>> userProfileResponseFutures = followers.stream()
				.map((Follower follower) -> {
					String followerId = follower.getId();
					return lineMessagingClient.getProfile(followerId)
							.whenComplete((UserProfileResponse response, Throwable throwable) -> {
								String followerDisplayName = response.getDisplayName();
								followerService.updateDisplayName(followerId, followerDisplayName);
							});
				});

		CompletableFuture<List<UserProfileResponse>> updateResult = FurtureUtils.sequence(userProfileResponseFutures);

		try {
			updateResult.get();
		} catch (InterruptedException e) {
			LogUtils.logThrowable(e);
			botMessageService.sendErrorLogToAdmins(e);
			Thread.currentThread().interrupt();
		} catch (ExecutionException e) {
			LogUtils.logThrowable(e);
			botMessageService.sendErrorLogToAdmins(e);
		}
	}
}
