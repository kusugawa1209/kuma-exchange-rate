package com.kuma.exchange.core.helper.command;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.vo.FlexMessageStyles;
import com.kuma.exchange.enums.CommandEnum;
import com.kuma.exchange.web.view.ThemeChoiceView;
import com.kuma.exchange.web.view.ThemePostBackView;
import com.kuma.utils.JsonUtils;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;

@Component
public class ThemeCommandHelper extends AbstractCommandHelper {

	@Override
	public CommandEnum getSupportedCommand() {
		return CommandEnum.THEME;
	}

	@Override
	public void handleCommand(String senderId, String messageWithoutCommand) {
		List<Bubble> themeBubbles = themeProperties.getThemes().stream().map((FlexMessageStyles theme) -> {
			Optional<FlexMessageStyles> stylesOptional = Optional.of(theme);
			Box header = flexMessageHelper.createHeader("標題文字", "標題敘述", stylesOptional);

			Text bodyText = flexMessageHelper.createBodyText("內文");

			ThemeChoiceView themeDemoView = new ThemeChoiceView(theme.getId(), false);
			ThemePostBackView postBackDemoView = new ThemePostBackView(themeDemoView);

			Button button = flexMessageHelper
					.createButton(new PostbackAction("按鈕", JsonUtils.toJsonString(postBackDemoView)), stylesOptional);

			Box body = flexMessageHelper.createBox(Lists.newArrayList(bodyText, button), FlexLayout.VERTICAL);

			ThemeChoiceView themeChoiceView = new ThemeChoiceView(theme.getId(), true);
			ThemePostBackView postBackView = new ThemePostBackView(themeChoiceView);
			
			Button selectThemeButton = flexMessageHelper
					.createButton(new PostbackAction("選擇此主題", JsonUtils.toJsonString(postBackView)), stylesOptional);
			Box footer = flexMessageHelper.createBox(Lists.newArrayList(selectThemeButton), FlexLayout.VERTICAL);

			return flexMessageHelper.createBubble(header, null, body, footer, stylesOptional);
		}).collect(Collectors.toList());

		List<FlexMessage> messages = flexMessageHelper.createFlexMessages(CommandEnum.THEME.getCommandText(),
				themeBubbles);

		botMessageService.pushThemeMessages(senderId, Lists.newArrayList(messages));
	}
}
