package com.kuma.exchange.core.retriver.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.enums.BankEnum;

@Service
@Component
@Transactional
public class CtBankRateRetriver extends AbstractJSoupRateRetriver {
	@Override
	public BankEnum getBank() {
		return BankEnum.CT_BANK;
	}

	@Override
	protected List<ExchangeRate> tryGetExchangeRates(Document document) throws IOException {
		System.err.println(document.toString());
		List<ExchangeRate> rates = Lists.newArrayList();

		Elements trs = document.select("table.twrbo-c-table.twrbo-c-table--zebra>tbody>tr");

		for (Element tr : trs) {
			Elements tds = tr.children();
			ExchangeRate rate = new ExchangeRate();
			for (Element td : tds) {
				String tdText = td.ownText();
				int tdIndex = tds.indexOf(td);

				switch (tdIndex) {
				case 0:
					rate.setCurrency(convertToCurrency(tdText));
					break;
				case 1:
					rate.setCashBuy(NumberUtils.toDouble(tdText));
					break;
				case 2:
					rate.setCashSell(NumberUtils.toDouble(tdText));
					break;
				case 3:
					rate.setSpotBuy(NumberUtils.toDouble(tdText));
					break;
				case 4:
					rate.setSpotSell(NumberUtils.toDouble(tdText));
					break;
				default:
					break;
				}
			}
			rate.setCreationDate(new Date());

			rate.setBank(getBank());
			rates.add(rate);
		}

		return rates;
	}
}
