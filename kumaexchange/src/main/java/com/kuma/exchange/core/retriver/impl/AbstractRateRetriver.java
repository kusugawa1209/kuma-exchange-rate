package com.kuma.exchange.core.retriver.impl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.core.retriver.RateRetriver;
import com.kuma.exchange.core.service.ExchangeRateService;
import com.kuma.exchange.enums.CurrencyEnum;
import com.kuma.utils.LogUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public abstract class AbstractRateRetriver implements RateRetriver {

	protected static final int MAX_RETRY_TIME = 5;

	@Autowired
	private ExchangeRateService exchangeRateService;

	protected abstract List<ExchangeRate> getExchangeRates() throws IOException;

	@Override
	public List<ExchangeRate> retriveRates() {
		log.info("Start retriving rates of {}", getBank().getDisplayName());
		List<ExchangeRate> rates = Lists.newArrayList();
		int tryTime = 0;
		boolean success = false;

		while (tryTime < MAX_RETRY_TIME && !success) {
			try {
				log.debug("Try for {} time", tryTime + 1);
				rates = getExchangeRates();
				success = true;
				log.debug("Try for {} time success", tryTime + 1);
			} catch (IOException e) {
				tryTime++;
				success = false;
				LogUtils.logThrowable(e, "Try for {} time failed", tryTime + 1);
			}
		}

		if (!success) {
			log.error("Retry over than {} times, give up.", MAX_RETRY_TIME);
		}

		return rates;
	}

	protected CurrencyEnum convertToCurrency(String currencyValue) {
		Optional<CurrencyEnum> currencyOptional = Lists.newArrayList(CurrencyEnum.values()).stream()
				.filter(currency -> StringUtils.containsIgnoreCase(currencyValue, currency.name())).findFirst();

		if (currencyOptional.isPresent()) {
			return currencyOptional.get();
		} else {
			throw new IllegalArgumentException("Not supported currency type: " + currencyValue);
		}
	}
}
