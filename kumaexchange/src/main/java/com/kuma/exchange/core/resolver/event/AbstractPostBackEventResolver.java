package com.kuma.exchange.core.resolver.event;

import org.springframework.beans.factory.annotation.Autowired;

import com.kuma.exchange.core.helper.message.FlexMessageHelper;
import com.kuma.exchange.properties.MessageProperties;
import com.kuma.exchange.properties.ThemeProperties;
import com.kuma.utils.JsonUtils;

public abstract class AbstractPostBackEventResolver<T> implements PostBackEventResolver {

	@Autowired
	protected MessageProperties messageProperties;

	@Autowired
	protected ThemeProperties themeProperties;

	@Autowired
	protected FlexMessageHelper flexMessageHelper;

	protected abstract Class<T> getEventDataViewClass();

	protected abstract void handleEvent(String senderId, T paramsView);

	private T resolve(String paramsContent) {
		return JsonUtils.toObject(paramsContent, getEventDataViewClass());
	}

	@Override
	public void handleEvent(String senderId, String paramsContent) {
		T paramsView = resolve(paramsContent);

		handleEvent(senderId, paramsView);
	}
}
