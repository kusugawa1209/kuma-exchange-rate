package com.kuma.exchange.core.helper.command;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.kuma.exchange.core.vo.FlexMessageStyles;
import com.kuma.exchange.enums.CommandEnum;
import com.linecorp.bot.model.message.FlexMessage;

@Component
public class UpdateRateCommandHelper extends AbstractCommandHelper {

	@Override
	public CommandEnum getSupportedCommand() {
		return CommandEnum.UPDATE_RATE;
	}

	@Override
	public void handleCommand(String senderId, String messageWithoutCommand) {
		exchangeRateService.updateRates();

		Optional<FlexMessageStyles> stylesOptional = getThemesByUserId(senderId);
		FlexMessage message = flexMessageHelper.createFlexMessage(messageProperties.getRateUpdated(),
				messageProperties.getUpdateSuccess(), messageProperties.getRateUpdated(), stylesOptional);

		botMessageService.pushMessage(senderId, message);
	}
}
