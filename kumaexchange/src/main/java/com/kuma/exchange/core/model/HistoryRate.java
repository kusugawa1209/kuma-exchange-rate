package com.kuma.exchange.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.kuma.exchange.enums.BankEnum;
import com.kuma.exchange.enums.CurrencyEnum;

import lombok.Data;

@Data
@Entity
@Table(name = "HISTORY_RATE")
public class HistoryRate {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "CURRENCY")
	private CurrencyEnum currency;

	@Column(name = "CASH_BUY")
	private Double cashBuy;

	@Column(name = "CASH_SELL")
	private Double cashSell;

	@Column(name = "SPOT_BUY")
	private Double spotBuy;

	@Column(name = "SPOT_SELL")
	private Double spotSell;

	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "BANK")
	private BankEnum bank;
}
