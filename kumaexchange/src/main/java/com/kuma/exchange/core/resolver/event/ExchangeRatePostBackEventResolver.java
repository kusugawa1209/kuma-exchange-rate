package com.kuma.exchange.core.resolver.event;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.exchange.core.helper.command.ExchangeRateCommandHelper;
import com.kuma.exchange.enums.PostBackTypeEnum;
import com.kuma.exchange.web.view.ExchangeRatePostBackView;

@Component
public class ExchangeRatePostBackEventResolver extends AbstractPostBackEventResolver<ExchangeRatePostBackView> {

	@Autowired
	private ExchangeRateCommandHelper exchangeRateCommandHelper;

	@Override
	public PostBackTypeEnum getSupportedPostBackEventType() {
		return PostBackTypeEnum.EXCHANGE_RATE;
	}

	@Override
	protected Class<ExchangeRatePostBackView> getEventDataViewClass() {
		return ExchangeRatePostBackView.class;
	}

	@Override
	protected void handleEvent(String senderId, ExchangeRatePostBackView paramsView) {
		List<String> currencies = paramsView.getParams();
		exchangeRateCommandHelper.handleCommand(senderId,
				StringUtils.join(currencies, messageProperties.getSetupSplit()));
	}

}
