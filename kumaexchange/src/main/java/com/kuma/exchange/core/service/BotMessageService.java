package com.kuma.exchange.core.service;

import java.util.List;

import com.linecorp.bot.model.message.Message;

public interface BotMessageService {

	public void replyMessage(String replyToken, Message message);

	public void replyMessages(String replyToken, List<Message> messages);

	public void pushMessage(String id, Message message);

	public void pushMessageToAdmins(Message message);

	public void pushMessageToAllUsers(Message message);

	public void pushMessages(String id, List<Message> messages);

	public void pushThemeMessages(String id, List<Message> messages);

	public void pushMessagesToAdmins(List<Message> messages);

	public void pushMessagesToAllUsers(List<Message> messages);

	public void sendErrorLogToAdmins(Throwable t);
}