package com.kuma.exchange.core.helper.command;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.kuma.exchange.core.helper.message.FlexMessageHelper;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.BotMessageService;
import com.kuma.exchange.core.service.ExchangeRateService;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.core.vo.FlexMessageStyles;
import com.kuma.exchange.enums.RoleEnum;
import com.kuma.exchange.properties.MessageProperties;
import com.kuma.exchange.properties.ThemeProperties;
import com.linecorp.bot.client.LineMessagingClient;

public abstract class AbstractCommandHelper implements CommandHelper {

	@Autowired
	protected LineMessagingClient lineMessagingClient;

	@Autowired
	protected FollowerService followerService;

	@Autowired
	protected BotMessageService botMessageService;

	@Autowired
	protected ExchangeRateService exchangeRateService;

	@Autowired
	protected MessageProperties messageProperties;

	@Autowired
	protected ThemeProperties themeProperties;

	@Autowired
	protected FlexMessageHelper flexMessageHelper;

	@Override
	public Set<RoleEnum> findSupportedRoles() {
		return getSupportedCommand().getRoles();
	}

	protected Optional<FlexMessageStyles> getThemesByUserId(String senderId) {
		Follower follower = followerService.getById(senderId);
		Long themeId = follower.getThemeId();

		return themeProperties.getById(themeId);
	}
}
