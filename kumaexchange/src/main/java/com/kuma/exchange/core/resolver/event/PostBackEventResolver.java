package com.kuma.exchange.core.resolver.event;

import com.kuma.exchange.enums.PostBackTypeEnum;

public interface PostBackEventResolver {

	public PostBackTypeEnum getSupportedPostBackEventType();

	public void handleEvent(String senderId, String paramsContent);
}
