package com.kuma.exchange.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kuma.exchange.core.dao.FollowerDao;
import com.kuma.exchange.core.model.Follower;
import com.kuma.exchange.core.service.FollowerService;
import com.kuma.exchange.enums.RoleEnum;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.UnfollowEvent;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
@Transactional
public class FollowerServiceImpl implements FollowerService {

	@Autowired
	private FollowerDao followerDao;

	@Override
	public List<Follower> findAll() {
		return followerDao.findAll();
	}

	@Override
	public void handleFollowEvent(FollowEvent event) {
		String userId = event.getSource().getUserId();
		saveFollower(userId);
	}

	@Override
	public void handleUnfollowEvent(UnfollowEvent event) {
		String userId = event.getSource().getUserId();
		removeFollower(userId);
	}

	@Override
	public void updateDisplayName(String userId, String displayName) {
		followerDao.updateDisplayName(userId, displayName);
	}

	@Override
	public List<Follower> findByDisplayName(String displayName) {
		return followerDao.findByDisplayName(displayName);
	}

	@Override
	public Follower getById(String userId) {
		return followerDao.findOne(userId);
	}

	@Override
	public List<Follower> findAdmins() {
		return followerDao.findByRole(RoleEnum.ADMIN);
	}

	@Override
	public boolean isAdmin(String userId) {
		Follower follower = followerDao.getByIdAndRole(userId, RoleEnum.ADMIN);

		return follower != null;
	}

	@Override
	public void updateTheme(String userId, Long themeId) {
		followerDao.updateTheme(userId, themeId);
	}

	private Follower saveFollower(String id) {
		Follower follower = followerDao.findOne(id);
		if (follower != null) {
			log.debug("Follower already existed. id: {}", id);

			return follower;
		}

		follower = new Follower();
		follower.setId(id);
		follower.setRole(RoleEnum.USER);

		return followerDao.save(follower);
	}

	private void removeFollower(String id) {
		Follower follower = followerDao.findOne(id);
		if (follower == null) {
			log.debug("Follower didn't exist. id: {}", id);
		} else {
			followerDao.delete(follower);
		}
	}
}
