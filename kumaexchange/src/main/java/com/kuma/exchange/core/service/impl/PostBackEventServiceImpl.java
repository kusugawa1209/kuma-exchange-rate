package com.kuma.exchange.core.service.impl;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.kuma.exchange.core.resolver.event.PostBackEventResolver;
import com.kuma.exchange.core.service.PostBackEventService;
import com.kuma.exchange.enums.PostBackTypeEnum;
import com.kuma.exchange.utils.ApplicationContextUtils;
import com.kuma.exchange.web.view.BasicPostBackView;
import com.kuma.utils.JsonUtils;
import com.linecorp.bot.model.event.PostbackEvent;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class PostBackEventServiceImpl implements PostBackEventService {

	@Override
	public void handleEvent(PostbackEvent event) {

		String postBackData = event.getPostbackContent().getData();

		BasicPostBackView postBackDataView = JsonUtils.toObject(postBackData,
				BasicPostBackView.class);
		PostBackTypeEnum eventType = postBackDataView.getEventType();

		Map<String, PostBackEventResolver> postBackEventResolvers = ApplicationContextUtils.getApplicationContext()
				.getBeansOfType(PostBackEventResolver.class);

		Stream<PostBackEventResolver> resolverStream = postBackEventResolvers.entrySet().stream()
				.map(Entry<String, PostBackEventResolver>::getValue);
		
		Map<PostBackTypeEnum, PostBackEventResolver> resolversMap = resolverStream
				.collect(Collectors.toMap(PostBackEventResolver::getSupportedPostBackEventType, resolver -> resolver));

		PostBackEventResolver resolver = resolversMap.get(eventType);
		if (resolver != null) {
			resolver.handleEvent(event.getSource().getSenderId(), postBackData);
		} else {
			log.warn("Unsupported eventType: {}", eventType);
		}
	}
}
