package com.kuma.exchange.core.retriver;

import java.util.List;

import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.exchange.enums.BankEnum;

public interface RateRetriver {

	public BankEnum getBank();
	
	public List<ExchangeRate> retriveRates();
}
