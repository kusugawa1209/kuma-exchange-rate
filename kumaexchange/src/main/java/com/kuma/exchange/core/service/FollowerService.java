package com.kuma.exchange.core.service;

import java.util.List;

import com.kuma.exchange.core.model.Follower;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.event.UnfollowEvent;

public interface FollowerService {

	public List<Follower> findAll();

	public void handleFollowEvent(FollowEvent event);

	public void handleUnfollowEvent(UnfollowEvent event);

	public void updateDisplayName(String userId, String displayName);

	public List<Follower> findByDisplayName(String displayName);

	public Follower getById(String userId);

	public List<Follower> findAdmins();

	public boolean isAdmin(String userId);

	public void updateTheme(String userId, Long themeId);
}