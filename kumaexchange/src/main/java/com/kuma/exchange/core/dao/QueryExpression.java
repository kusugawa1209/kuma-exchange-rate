package com.kuma.exchange.core.dao;

import org.apache.commons.lang3.StringUtils;

public class QueryExpression {
	
	private static final String LIKE_SYMBOL = "%";

	public static String appendLikeEncode(String value) {
		
		if ( StringUtils.isBlank(value) ) {
			return StringUtils.EMPTY;
		}
		
        value = value.replaceAll(LIKE_SYMBOL, "");
        return LIKE_SYMBOL + value + LIKE_SYMBOL;
    }

    public static String appendLikeEncodeAndUpperCase(String value) {
    	
		if ( StringUtils.isBlank(value) ) {
			return StringUtils.EMPTY;
		}
    	
        value = StringUtils.upperCase(value);
        return appendLikeEncode(value);
    }
    
	public static String appendEndLikeEncode(String value) {
		if (StringUtils.isBlank(value)) {
			return StringUtils.EMPTY;
		}

		value = value.replaceAll(LIKE_SYMBOL, StringUtils.EMPTY);

		return LIKE_SYMBOL + value;
	}

}
