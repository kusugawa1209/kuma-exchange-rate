package com.kuma.exchange.core.retriver.impl;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.common.collect.Lists;
import com.kuma.exchange.core.model.ExchangeRate;
import com.kuma.utils.LogUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public abstract class AbstractHtmlUnitRateRetriver extends AbstractRateRetriver {

	private static final int JS_TIMEOUT_MILLS = 10000;
	private static final int BACKGROUND_JS_TIMEOUT_MILLS = 30000;

	protected abstract List<ExchangeRate> tryGetExchangeRates(Document document) throws IOException;

	@Override
	protected List<ExchangeRate> getExchangeRates() throws IOException {

		try (WebClient webClient = new WebClient(BrowserVersion.CHROME)) {
			webClient.getOptions().setJavaScriptEnabled(true);
			webClient.getOptions().setCssEnabled(false);
			webClient.setJavaScriptTimeout(JS_TIMEOUT_MILLS);
			webClient.waitForBackgroundJavaScript(BACKGROUND_JS_TIMEOUT_MILLS);
			webClient.getOptions().setTimeout(BACKGROUND_JS_TIMEOUT_MILLS);
			webClient.getOptions().setThrowExceptionOnScriptError(false);
			webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
			webClient.setAjaxController(new NicelyResynchronizingAjaxController());

			HtmlPage rootPage = (HtmlPage) webClient.getCurrentWindow().getEnclosedPage();
			rootPage = webClient.getPage(getBank().getUri());

			Document document = Jsoup.parse(rootPage.asXml());

			return tryGetExchangeRates(document);
		} catch (IOException e) {
			LogUtils.logThrowable(e);
			throw e;
		}
	}

	@Override
	public List<ExchangeRate> retriveRates() {
		log.info("Start retriving rates of {}", getBank().getDisplayName());
		List<ExchangeRate> rates = Lists.newArrayList();
		int tryTime = 0;
		boolean success = false;

		while (tryTime < MAX_RETRY_TIME && !success) {
			try {
				log.debug("Try for {} time", tryTime + 1);
				rates = getExchangeRates();
				success = true;
				log.debug("Try for {} time success", tryTime + 1);
			} catch (IOException e) {
				tryTime++;
				success = false;
				LogUtils.logThrowable(e, "Try for {} time failed", tryTime + 1);
				log.debug(e.toString());
			}
		}

		if (!success) {
			log.error("Retry over than {} times, give up.", MAX_RETRY_TIME);
		}

		return rates;
	}
}
