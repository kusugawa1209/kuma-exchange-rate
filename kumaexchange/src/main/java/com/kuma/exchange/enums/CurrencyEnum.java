package com.kuma.exchange.enums;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;

import lombok.Getter;

public enum CurrencyEnum {
	USD("美金", 1, true),
	JPY("日圓", 2, true), 
	EUR("歐元", 3, true), 
	AUD("澳幣", 4, true), 
	CNY("人民幣", 5, true), 
	GBP("英鎊", 6, false),
	KRW("韓元", 7, false), 
	NZD("紐元", 8, false), 
	CAD("加拿大幣", 9, false), 
	CHF("瑞士法郎", 10, false), 
	HKD("港幣", 11, false), 
	IDR("印尼盾", 12, false), 
	INR("印度盧比", 13, false), 
	MOP("澳門幣", 14, false),
	MYR("馬來幣", 15, false),
	PHP("菲國比索", 16, false), 
	SEK("瑞典幣", 17, false), 
	SGD("新加坡幣", 18, false), 
	THB("泰幣", 19, false), 
	VND("越南盾", 20, false), 
	ZAR("南非幣", 21, false);

	@Getter
	private String displayName;

	@Getter
	private boolean isDefault;
	
	@Getter
	private int sorting;

	private CurrencyEnum(String displayName, int sorting, boolean isDefault) {
		this.displayName = displayName;
		this.sorting = sorting;
		this.isDefault = isDefault;
	}

	public List<CurrencyEnum> findDefaultCurrencies() {
		return Lists.newArrayList(CurrencyEnum.values()).stream().filter(CurrencyEnum::isDefault)
				.collect(Collectors.toList());
	}

	public String getFullName() {
		return String.format("%s (%s)", this.getDisplayName(), this.name());
	}

	public static List<CurrencyEnum> findByNamesOrDisplayNames(String... currencies) {
		return Lists.newArrayList(CurrencyEnum.values()).stream()
				.filter(currency -> Lists.newArrayList(currencies).stream()
						.anyMatch(currencyText -> StringUtils.contains(currencyText, currency.name())
								|| StringUtils.contains(currencyText, currency.getDisplayName())))
				.collect(Collectors.toList());
	}
}
