package com.kuma.exchange.enums;

import lombok.Getter;

public enum MessageContentTypeEnum {

	IMAGE("圖片"), LOCATION("位置資訊"), AUDIO("音訊"), VIDEO("影片");

	@Getter
	private String contentDisplayName;

	private MessageContentTypeEnum(String contentDisplayName) {
		this.contentDisplayName = contentDisplayName;
	}
}
