package com.kuma.exchange.enums;

import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.Getter;

public enum CommandEnum {
	FOLLOWERS("使用者清單", Sets.newHashSet(RoleEnum.ADMIN), true), 
	UPDATE_RATE("更新匯率", Sets.newHashSet(RoleEnum.ADMIN), true),
	TRANSFER("傳送訊息", Sets.newHashSet(RoleEnum.ADMIN, RoleEnum.USER), true),
	EXCHANGE_RATE("匯率資訊", Sets.newHashSet(RoleEnum.ADMIN, RoleEnum.USER), true),
	HELP("HELP", Sets.newHashSet(RoleEnum.ADMIN, RoleEnum.USER), false),
	THEME("選擇佈景主題", Sets.newHashSet(RoleEnum.ADMIN, RoleEnum.USER), true);

	private CommandEnum(String commandText, Set<RoleEnum> roles, boolean isShowInMenu) {
		this.commandText = commandText;
		this.roles = roles;
		this.isShowInMenu = isShowInMenu;
	}

	@Getter
	private String commandText;

	@Getter
	private Set<RoleEnum> roles;

	@Getter
	private boolean isShowInMenu;

	public static Set<CommandEnum> findCommandsByRole(RoleEnum role) {
		return Lists.newArrayList(CommandEnum.values()).stream().filter(x -> x.getRoles().contains(role))
				.collect(Collectors.toSet());
	}

}
