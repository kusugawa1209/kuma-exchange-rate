package com.kuma.exchange.enums;

public enum PostBackTypeEnum {
	EXCHANGE_RATE, COMMAND, THEME, CONVERSATION;
}
