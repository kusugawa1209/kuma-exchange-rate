package com.kuma.exchange.enums;

import org.apache.commons.lang3.StringUtils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum BankEnum {
	CT_BANK("中國信託", "https://www.ctbcbank.com/twrbo/zh_tw/dep_index/dep_ratequery/dep_foreign_rates.html", 1),
	BOT_BANK("臺灣銀行", "https://rate.bot.com.tw/xrt?Lang=zh-TW", 2),
	TAISHIN_BANK("台新銀行", "https://www.taishinbank.com.tw/TS/TS06/TS0605/TS060502/index.htm?urlPath1=TS02&urlPath2=TS0202", 3),
	MEGA_BANK("兆豐銀行", "https://wwwfile.megabank.com.tw/other/bulletin02_02.asp", 4);

	@Getter
	private String displayName;

	@Getter
	private String uri;

	@Getter
	private int order;

	public static BankEnum getByBankName(String bankName) {
		if (StringUtils.isBlank(bankName)) {
			return null;
		}

		for (BankEnum bank : BankEnum.values()) {
			if (bank.getDisplayName().equals(bankName)) {
				return bank;
			}
		}

		return null;
	}
}
